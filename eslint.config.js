const config = require('./packages/eslint-config/src/eslint.config');

module.exports = [
	config,
  {
    ignores: [
      "/**/node_modules/*",
      "/**/dist/*",
      "/**/src/*.d.ts"
    ]
  }
];
