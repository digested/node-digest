/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  type ConfigAPI,
  type PluginTarget,
  type TransformOptions
  // eslint-disable-next-line n/no-extraneous-import
} from '@babel/core';
import {
  config
} from '@digest/scripts';

const DISABLE_PLUGINS: { [key: string]: boolean } = {
  '@babel/plugin-syntax-dynamic-import': true,
  '@babel/plugin-transform-regenerator': true,
  '@babel/plugin-transform-typescript': true,
  '@dr.pogodin/babel-plugin-react-css-modules': true,
  'babel-plugin-react-remove-properties': true,
  'babel-plugin-transform-inline-environment-variables': true,
  'babel-plugin-transform-node-env-inline': true
};
const DISABLE_PRESETS: { [key: string]: boolean } = {
  'babel-preset-minify': true
};

const babelPrep = function (
  api: ConfigAPI
): TransformOptions {
  let babelConfig: TransformOptions | undefined;

  if (config.babel) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
    babelConfig = require(config.babel)(api);
  }

  if (babelConfig) {
    const preparedBabelConfig = {
      ...babelConfig,

      plugins: babelConfig.plugins ?
        babelConfig.plugins.filter((plugin): boolean => {
          if (Array.isArray(plugin)) {
            return !DISABLE_PLUGINS[plugin[0] as string];
          }

          return false;
        }) :
        [],
      presets: babelConfig.presets ?
        babelConfig.presets.filter(
          (preset): boolean => {
            if (Array.isArray(preset)) {
              return !DISABLE_PRESETS[preset[0] as string];
            }

            return false;
          }
        ) :
        []

    };

    const presetIndex = preparedBabelConfig.presets.findIndex(
      (preset): boolean => {
        if (Array.isArray(preset)) {
          return preset[0] === '@babel/preset-env';
        }

        return false;
      }
    );

    if (
      config.cssModules &&
      config.react
    ) {
      preparedBabelConfig.plugins.push(
        [
          '@dr.pogodin/babel-plugin-react-css-modules',
          {
            context: config.context,
            generateScopedName: '[local]'
          }
        ]
      );
    }

    if (
      config.babelPolyfills &&
      presetIndex > -1
    ) {
      (
        preparedBabelConfig.presets[presetIndex] as [PluginTarget, { modules: string }]
      )[1].modules = 'commonjs';

      (
        preparedBabelConfig.presets[presetIndex] as [PluginTarget, { targets: string }]
      )[1].targets = 'node 11';
    } else {
      preparedBabelConfig.plugins.unshift([
        '@babel/plugin-transform-modules-commonjs'
      ]);
    }

    return preparedBabelConfig;
  }

  return {};
};

// module.exports must be used to play nice with some
// other babel config consumers (eg. Jest)

// eslint-disable-next-line unicorn/prefer-module
module.exports = babelPrep;
