/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prefer-module
*/

const canonicalJSXAlly = require('eslint-config-canonical/configurations/jsx-a11y');
const canonicalReact = require('eslint-config-canonical/configurations/react');
const cssModulesPlugin = require('eslint-plugin-css-modules');
const reactPlugin = require('eslint-plugin-react');

const cssModulesPluginFixed = {
  ...cssModulesPlugin.configs.recommended,
  plugins: {
    'css-modules': cssModulesPlugin
  }
};

const reactConfigs = [
  cssModulesPluginFixed,
  {
    ...reactPlugin.configs.flat.recommended,
    settings: {
      react: {
        version: 'detect',
      },
    }
  },
  {
    ...canonicalReact.recommended,
    files: [
      '**/*.{js,ts,jsx,tsx}'
    ],
    rules: {
      ...canonicalReact.recommended.rules,
      'react/forbid-component-props': 'off',
      'react/no-unknown-property': [
        'error',
        {
          ignore: [
            'styleName'
          ]
        }
      ]
    },
    settings: {
      react: {
        version: 'detect',
      },
    }
  },
  canonicalJSXAlly.recommended
];

module.exports = reactConfigs;
