/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prefer-module
*/

const config = require('@digest/eslint-config');

module.exports = config;
