/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prevent-abbreviations
*/
import {
  type NextHandleFunction
  // eslint-disable-next-line n/no-extraneous-import
} from 'connect';
import {
  type Context
  // eslint-disable-next-line n/no-extraneous-import
} from 'koa';
import {
  type IncomingMessage,
  type ServerResponse
} from 'node:http';
import {
  type Compiler
  // eslint-disable-next-line n/no-extraneous-import
} from 'webpack';
import devMiddleware, {
  type Options
} from 'webpack-dev-middleware';

const webpackDevMiddleware = (
  compiler: Compiler,
  options?: Options<IncomingMessage, ServerResponse>
) => {
  const expressMiddleware = devMiddleware(
    compiler,
    options
  );

  // eslint-disable-next-line func-style
  async function middleware (
    context: Context,
    next: NextHandleFunction
  ) {
    const {
      locals,
      req: request,
      state
    } = context;

    await expressMiddleware(
      request,
      {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        end: (content) => {
          context.body = content;
        },
        getHeader: context.get.bind(context),
        locals: locals || state,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        setHeader: context.set.bind(context)
      },
      next
    );
  }

  return middleware;
};

export default webpackDevMiddleware;
