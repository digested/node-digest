/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  type Middleware
  // eslint-disable-next-line n/no-extraneous-import
} from 'koa';
import {
  PassThrough
} from 'node:stream';
import {
  type Compiler
  // eslint-disable-next-line n/no-extraneous-import
} from 'webpack';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import expressHotMiddleware from 'webpack-hot-middleware';

const webpackHotMiddleware = (
  compiler: Compiler,
  options?: { [key: string]: unknown }
) => {
  const middleware = expressHotMiddleware(
    compiler,
    options
  );

  const expressMiddleware: Middleware = async (
    context,
    next
  ) => {
    const {
      req: request
    } = context;

    const stream = new PassThrough();

    await middleware(
      request,
      {
        end: (
          content: unknown
        ) => {
          context.body = content;
        },
        write: stream.write.bind(stream),
        writeHead (
          status: number,
          headers: {
            [key: string]: string | string[];
          }
        ) {
          context.body = stream;
          context.status = status;
          context.set(headers);
        }
      },
      next
    );
  };

  return expressMiddleware;
};

export default webpackHotMiddleware;
