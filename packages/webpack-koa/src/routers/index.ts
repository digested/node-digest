/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prevent-abbreviations
*/
import webpack from '../util/webpack';
import dev from './dev';
import hot from './hot';
import history from '@digest/koa/dist/routers/history';
import statics from '@digest/koa/dist/routers/statics';
import {
  config,
  production
} from '@digest/scripts';
// eslint-disable-next-line n/no-extraneous-import, import/no-extraneous-dependencies
import koaRouter from '@koa/router';

const routers = () => {
  const {
    baseHref
  } = config;

  const staticPath = config.staticPath;

  const router = new koaRouter({
    sensitive: true,
    strict: true
  });

  const historyRouter = history(
    baseHref
  );

  const staticsRouter = statics(
    baseHref,
    staticPath,
    production
  );

  router.use(
    baseHref,
    historyRouter.routes(),
    historyRouter.allowedMethods(),
    staticsRouter.routes(),
    staticsRouter.allowedMethods()
  );

  /**
   * On Koa, hot and dev must come after history and statics
   */
  if (!production) {
    const compiler = webpack();

    const devRouter = dev(
      compiler,
      baseHref
    );

    const hotRouter = hot(
      compiler,
      baseHref
    );

    router.use(
      baseHref,
      devRouter.routes(),
      devRouter.allowedMethods(),
      hotRouter.routes(),
      hotRouter.allowedMethods()
    );
  }

  return router;
};

export default routers;
