/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import webpackHotMiddleware from '../middleware/webpackHotMiddleware';
// eslint-disable-next-line n/no-extraneous-import, import/no-extraneous-dependencies
import Router from '@koa/router';
import path from 'node:path';
import {
  type Compiler
  // eslint-disable-next-line n/no-extraneous-import
} from 'webpack';

const hot = (
  compiler: Compiler,
  baseHref: string,
  hotPath = 'hot'
) => {
  const router = new Router({
    sensitive: true,
    strict: true
  });

  const hotMiddleware = webpackHotMiddleware(
    compiler,
    {
      path: `${path.join(
        baseHref
      )}${hotPath}`
    }
  );

  const hotBase = '(.*)';

  router.get(
    hotBase,
    hotMiddleware
  );

  router.head(
    hotBase,
    hotMiddleware
  );

  return router;
};

export default hot;
