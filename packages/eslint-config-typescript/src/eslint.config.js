/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prefer-module,
  no-unused-vars
*/

const canonicalTypeScript = require('eslint-config-canonical/configurations/typescript');
const canonicalCompatibility = require('eslint-config-canonical/configurations/typescript-compatibility');
const canonicalTypeScriptTypeChecking = require('eslint-config-canonical/configurations/typescript-type-checking');
const importPlugin = require('eslint-plugin-import');
const tseslint = require('typescript-eslint');

const {
  '@typescript-eslint/func-call-spacing': functionCallSpacing,
  '@typescript-eslint/indent': indent,
  '@typescript-eslint/object-curly-spacing': objectCurlySpacing,
  '@typescript-eslint/type-annotation-spacing': typeAnnotationSpacing,
  ...canonicalTypeScriptFixedRules
} = canonicalTypeScript.recommended.rules;

const {
  '@typescript-eslint/func-call-spacing': functionCallSpacing2,
  '@typescript-eslint/indent': indent2,
  ...canonicalCompatibilityFixedRules
} = canonicalCompatibility.recommended.rules;

const typescriptConfigs = tseslint.config(
  {
    extends: [
      importPlugin.flatConfigs.typescript,
      ...tseslint.configs.strictTypeChecked,
      ...tseslint.configs.stylisticTypeChecked,
      {
        ...canonicalTypeScript.recommended,
        rules: {
          ...canonicalTypeScriptFixedRules,
          '@stylistic/member-delimiter-style': [
            2,
            {
              multiline: {
                delimiter: 'semi',
                requireLast: true,
              },
              overrides: {
                interface: {
                  multiline: {
                    delimiter: 'semi',
                    requireLast: true,
                  },
                },
              },
              singleline: {
                delimiter: 'semi',
                requireLast: false,
              },
            },
          ],
        }
      },
      {
        ...canonicalTypeScriptTypeChecking.recommended
      },
      {
        ...canonicalCompatibility.recommended,
        rules: canonicalCompatibilityFixedRules
      },
      {
        rules: {
          '@typescript-eslint/no-unused-expressions': 'off'
        }
      }
    ],
    files: [
      '**/*.{ts,tsx}'
    ],
    languageOptions: {
      parserOptions: {
        projectService: true,
        tsconfigRootDir: __dirname
      }
    }
  }
);

module.exports = typescriptConfigs;
