/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import threadLoader from './threadLoader';
// eslint-disable-next-line import/extensions
import type configuration from './webpack.config';
import {
  config,
  production
} from '@digest/scripts';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

type Loader = typeof configuration.loader;

const styleLoaders = (
  options: {
    camelCase?: boolean;
    minimize?: boolean;
    modules?: boolean;
    postcss?: boolean;
    production?: boolean;
    sourceMap?: boolean;
  } = {}
): Loader[] => {
  // eslint-disable-next-line @typescript-eslint/prefer-nullish-coalescing
  options.camelCase = options.camelCase === undefined ?
    true :
    options.camelCase;

  return ([] as Loader[]).concat(
    production ?
      [
        {
          loader: MiniCssExtractPlugin.loader as string,
          options: {
            publicPath: '../'
          }
        }
      ] :
      [
        threadLoader,
        {
          loader: 'style-loader'
        }
      ],
    [
      {
        loader: 'css-loader',
        options: {
          importLoaders: 1,
          modules: options.modules ?
            {
              exportLocalsConvention: options.camelCase ?
                'camelCase' :
                'asIs',
              localIdentName: config.library ?
                `${config.shortName}-[local]` :
                `[local]-[hash:${config.hashLimit}]`
            } :
            undefined,
          sourceMap: options.sourceMap
        }
      }
    ],
    options.postcss ?
      [
        {
          loader: 'postcss-loader',
          options: {
            postcssOptions: {
              ctx: {},
              path: config.webpackPostCSS
            },
            sourceMap: options.sourceMap
          }
        }
      ] :
      []
  );
};

export default styleLoaders;
