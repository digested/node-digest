/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  @typescript-eslint/ban-ts-comment,
  @typescript-eslint/no-non-null-assertion,
  @typescript-eslint/no-require-imports,
  @typescript-eslint/no-var-requires,
  canonical/filename-match-exported,
  unicorn/prefer-module
*/
import scriptLoaders from './scriptLoaders';
import styleLoaders from './styleLoaders';
import {
  config,
  production,
  package as project
} from '@digest/scripts';
// @ts-expect-error
import PreloadWebpackPlugin from '@vue/preload-webpack-plugin';
// @ts-expect-error
import CaseSensitivePathsPlugin from 'case-sensitive-paths-webpack-plugin';
// @ts-expect-error
import CircularDependencyPlugin from 'circular-dependency-plugin';
// @ts-expect-error
import CleanTerminalWebpackPlugin from 'clean-terminal-webpack-plugin';
import {
  CleanWebpackPlugin
} from 'clean-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
// import DotEnvDefinePlugin from 'dotenv-webpack';
// @ts-expect-error
import DuplicatePackageCheckerPlugin from 'duplicate-package-checker-webpack-plugin';
// @ts-expect-error
import HtmlWebpackHarddiskPlugin from 'html-webpack-harddisk-plugin';
import {
  HtmlWebpackLinkTypePlugin
} from 'html-webpack-link-type-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'node:path';
import v8 from 'node:v8';
import TerserPlugin from 'terser-webpack-plugin';
import {
  type Configuration,
  DefinePlugin,
  HotModuleReplacementPlugin,
  NoEmitOnErrorsPlugin,
  optimize
} from 'webpack';
import {
  BundleAnalyzerPlugin
} from 'webpack-bundle-analyzer';
// @ts-expect-error
// eslint-disable-next-line import/default
import WebpackFavicons from 'webpack-favicons';
// @ts-expect-error
import nodeExternals from 'webpack-node-externals';
import {
  SubresourceIntegrityPlugin
} from 'webpack-subresource-integrity';

const {
  cache
} = config;

let GenerateSW;
let WorkboxEntry;

if (
  !config.library &&
  config.pwa &&
  config.webpackWorkbox
) {
  // eslint-disable-next-line n/no-extraneous-require,import/no-extraneous-dependencies
  const WorkboxWebpackPlugin = require('workbox-webpack-plugin');

  GenerateSW = WorkboxWebpackPlugin.GenerateSW;

  WorkboxEntry = require.resolve('@digest/webpack-workbox');
}

let babelPolyfills: null | string = null;

if (
  config.babel &&
  config.babelPolyfills
) {
  babelPolyfills = require.resolve(config.babelPolyfills);
}

let ESLintPlugin;

if (
  config.eslint &&
  config.webpackESLint
) {
  // eslint-disable-next-line n/no-extraneous-require,import/no-extraneous-dependencies
  ESLintPlugin = require('eslint-webpack-plugin');
}

let angularPolyfills: null | string = null;

if (
  config.angular &&
  config.webpackAngular
) {
  angularPolyfills = require.resolve('@digest/angular/src/angularPolyfills.js');
}

let ForkTsCheckerWebpackPlugin;

if (
  config.typescript &&
  config.webpackTypeScript
) {
  // eslint-disable-next-line n/no-extraneous-require,import/no-extraneous-dependencies
  ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
}

let ReactRefreshWebpackPlugin;

if (
  !production &&
  config.react &&
  config.webpackReact
) {
  // eslint-disable-next-line n/no-extraneous-require,import/no-extraneous-dependencies
  ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
}

const generateName = (
  extension: string,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unused-vars
  chunkName?: any
): string => {
  const hash = extension === 'css' ?
    `[contenthash:${config.hashLimit}]` :
    `[chunkhash:${config.hashLimit}]`;

  if (config.library) {
    if (production) {
      if (config.minimize) {
        return `[name].min.${extension}`;
      }

      return `[name].${extension}`;
    }

    return `[name].${extension}`;
  }

  if (production) {
    return `[name].${extension}?_=${hash}`;
  }

  return `[name].${extension}`;
};

const baseHrefHtml = config.baseHrefHtml ?
  config.baseHrefHtml.replace(
    /\/?$/u,
    '/'
  ) :
  // eslint-disable-next-line @stylistic/no-extra-parens
  (
    config.baseHref ?
      config.baseHref.replace(
        /\/?$/u,
        '/'
      ) :
      undefined
  );

const configuration: Configuration = {
  cache: cache ?
    {
      cacheDirectory: path.join(
        cache,
        'webpack',
        config.reactNative && config.webpackReactNative ?
          'native' :
          'web'
      ),
      type: 'filesystem'
    } :
    false,
  context: config.context,
  devtool: config.sourceMaps ?
    production ?
      undefined :
      // eslint-disable-next-line unicorn/no-nested-ternary
      config.library ?
        'source-map' :
        'inline-source-map' :
    'nosources-source-map',
  entry: {
    main: ([] as string[]).concat(
      production || config.library ?
        [] :
        [
          path.join(
            'webpack',
            'hot',
            'dev-server'
          ),
          path.join(
            'webpack-hot-middleware',
            'client'
          ) + `?path=${path.join(baseHrefHtml ?? '/')}hot`
        ],
      !config.library &&
        config.babel &&
        babelPolyfills ?
        babelPolyfills :
        [],
      !config.library &&
        config.angular &&
        angularPolyfills ?
        angularPolyfills! :
        [],
      !config.library && WorkboxEntry && production ?
        WorkboxEntry :
        [],
      [
        config.context
      ]
    )
  },
  externals: config.library ?
    [
      nodeExternals()
    ] :
    undefined,
  externalsPresets: config.library ?
    {
      node: true
    } :
    undefined,
  mode: production ?
    'production' :
    'development',
  module: {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    rules: ([] as any[]).concat(
      [
        {
          generator: {
            filename: `templates/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.html$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `icons/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.ico$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `images/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.(png|jpg)$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `gifs/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.gif$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `svg/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.(svg)(\?v=\d\.\d\.\d)?$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `fonts/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.woff(2)?(\?v=\d\.\d\.\d)?$/u,
          type: 'asset/resource'
        },
        {
          generator: {
            filename: `fonts/[name][ext]?_=[hash:${config.hashLimit}]`
          },
          test: /\.(ttf|eot)(\?v=\d\.\d\.\d)?$/u,
          type: 'asset/resource'
        },
        {
          exclude: new RegExp(
            // eslint-disable-next-line no-useless-escape
            `\.g\.css$|\.min\.css$|node_modules\/(?!.*(${config.postcssWhitelist})).*\.css$`,
            'u'
          ),
          test: /\.css$/u,
          use: styleLoaders({
            minimize: config.minimize && production,
            modules: config.cssModules,
            postcss: Boolean(
              config.postcss &&
              config.webpackPostCSS
            ),
            sourceMap: config.sourceMaps
          })
        },
        {
          exclude: new RegExp(
            // eslint-disable-next-line no-useless-escape
            `\.min\.css$|node_modules\/(?!.*(${config.postcssWhitelist})).*\.css$`,
            'u'
          ),
          test: /\.g\.css$/u,
          use: styleLoaders({
            minimize: config.minimize && production,
            postcss: Boolean(
              config.postcss &&
              config.webpackPostCSS
            ),
            sourceMap: config.sourceMaps
          })
        },
        {
          test: new RegExp(
            // eslint-disable-next-line no-useless-escape
            `\.min\.css$|node_modules\/(?!.*(${config.postcssWhitelist})).*\.css$`,
            'u'
          ),

          use: styleLoaders({
            camelCase: false,
            minimize: false
          })
        },
        {
          exclude: /(node_modules|lib|dist)[\\/]/u,
          test: /\.js$/u,
          use: scriptLoaders()
        },
        {
          exclude: /(node_modules|lib|dist)[\\/]/u,
          test: /\.mjs$/u,
          use: scriptLoaders()
        }
      ],
      config.react &&
        config.webpackReact ?
        [
          {
            exclude: /(node_modules|lib|dist)[\\/]/u,
            test: /\.jsx$/u,
            use: scriptLoaders()
          }
        ] :
        [],
      config.typescript &&
        config.webpackTypeScript ?
        [
          {
            exclude: /(node_modules|lib|dist)[\\/]/u,
            test: /\.ts$/u,
            use: scriptLoaders({
              typescript: true
            })
          }
        ] :
        [],
      config.react &&
        config.webpackReact &&
        config.typescript &&
        config.webpackTypeScript ?
        [
          {
            exclude: /(node_modules|lib|dist)[\\/]/u,
            test: /\.tsx$/u,
            use: scriptLoaders({
              typescript: true
            })
          }
        ] :
        [],
      config.graphql &&
        config.webpackGraphQL ?
        [
          {
            exclude: /node_modules/u,
            test: /\.(graphql|gql)$/u,

            use: [
              {
                loader: 'graphql-tag/loader'
              }
            ]
          }
        ] :
        []
    )
  },
  optimization: {
    minimize: config.minimize && production,
    minimizer: [
      new TerserPlugin({
        extractComments: false,
        parallel: config.parallel,
        terserOptions: {
          compress: true,
          ecma: undefined,
          ie8: false,
          keep_classnames: false,
          keep_fnames: false,
          mangle: true,
          module: true,
          nameCache: {},
          output: {
            comments: false
          },
          safari10: false,
          sourceMap: config.sourceMaps,
          toplevel: true
        }
      }),
      '...',
      new CssMinimizerPlugin({
        minimizerOptions: {
          preset: [
            'default',
            {
              discardComments: {
                removeAll: true
              }
            }
          ]
        },
        parallel: config.parallel,
        warningsFilter: () => {
          return true;
        }
      })
    ],
    moduleIds: production ?
      undefined :
      'named',
    /* Breaks things */
    providedExports: false,
    runtimeChunk: config.library ?
      false :
      {
        name: (
          // eslint-disable-next-line @typescript-eslint/no-explicit-any
          entrypoint: any
        ): string => {
          return `runtime.${entrypoint.name}`;
        }
      },
    splitChunks: config.library ?
      // eslint-disable-next-line @stylistic/object-curly-newline
      {
        // name: true
      // eslint-disable-next-line @stylistic/object-curly-newline
      } :
      {
        chunks: 'all'
      }
  },
  output: {
    chunkFilename: config.library ?
      path.join(generateName('js')) :
      path.join(
        'scripts',
        'chunks',
        generateName('js')
      ),
    crossOriginLoading: 'anonymous',
    devtoolModuleFilenameTemplate: (
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      info: any
    ): string => {
      return `file://${path.resolve(info.absoluteResourcePath).replace(
        '\\',
        '/'
      )}`;
    },
    filename: (
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      chunkData: any
    ): string => {
      return config.library ?
        path.join(
          generateName(
            'js',
            chunkData
          )
        ) :
        path.join(
          'scripts',
          generateName(
            'js',
            chunkData
          )
        );
    },
    library: config.library ?
      'index' :
      undefined,
    libraryTarget: 'umd',
    path: path.resolve(
      config.staticPath,
      config.library ?
        '' :
        'bundle'
    ),
    publicPath: config.library ?
      './' :
      './bundle/',
    umdNamedDefine: true
  },

  performance: {
    hints: false
  },
  plugins: [
    new CleanTerminalWebpackPlugin({
      skipFirstRun: true
    }),
    new DuplicatePackageCheckerPlugin({
      emitError: true,
      strict: true,
      verbose: true
    }),
    new CircularDependencyPlugin({
      exclude: /node_modules/u
    }),
    new CaseSensitivePathsPlugin(),
    new DefinePlugin({
      __DEV__: !production,
      'process.env': {
        BASE_HREF: config.baseHrefHtml ?
          JSON.stringify(config.baseHrefHtml) :
          undefined,
        BUILD: {
          DATE: JSON.stringify(new Date().toString())
        },
        NODE_ENV: JSON.stringify(production ?
          'production' :
          'development'),

        ...config.library ?
          undefined :
          {
            PACKAGE: {
              DESCRIPTION: JSON.stringify(project.description),
              NAME: JSON.stringify(project.name),
              TITLE: JSON.stringify(project.title),
              VERSION: JSON.stringify(project.version)
            }
          },
        ...config.env
      }
    })
    /*
    ,
    new DotEnvDefinePlugin({
      ignoreStubs: true,
      silent: false
    })
    */
  ].concat(
    config.eslint &&
    config.webpackESLint ?
      [
        new ESLintPlugin({
          failOnError: production,
          failOnWarning: production,
          formatter: 'eslint-formatter-friendly',
          overrideConfigFile: config.eslint
        })
      ] :
      [],
    production ?
      [
        new optimize.ModuleConcatenationPlugin(),
        // new optimize.OccurrenceOrderPlugin(true),
        new MiniCssExtractPlugin({
          chunkFilename: config.library ?
            path.join(generateName('css')) :
            path.join(
              'styles',
              'chunks',
              generateName('css')
            ),
          filename: config.library ?
            path.join(generateName('css')) :
            path.join(
              'styles',
              generateName('css')
            )
        }),
        new BundleAnalyzerPlugin({
          analyzerMode: 'static',
          generateStatsFile: true,
          logLevel: 'silent',
          openAnalyzer: false,
          reportFilename: config.reports ?
            path.resolve(
              config.reports,
              'webpack',
              'index.html'
            ) :
            undefined,
          statsFilename: config.reports ?
            path.resolve(
              config.reports,
              'webpack',
              'index.json'
            ) :
            undefined,
          statsOptions: {
            publicPath: !production
          }
        })
      ] :
      [
        new CleanWebpackPlugin(),
        new HotModuleReplacementPlugin()
      ],
    !production &&
      config.react &&
      config.webpackReact ?
      [
        new ReactRefreshWebpackPlugin()
      ] :
      [],
    production ?
      [] :
      [
        new NoEmitOnErrorsPlugin()
        // new NamedModulesPlugin()
      ],
    config.typescript &&
      config.webpackTypeScript &&
      config.parallel ?
      [
        new ForkTsCheckerWebpackPlugin(
          {
            typescript: {
              configFile: config.typescript,
              memoryLimit: Math.floor(v8.getHeapStatistics().heap_size_limit / 1_000_000)
            }
          // workers: ForkTsCheckerWebpackPlugin.TWO_CPUS_FREE
          // eslint-disable-next-line @typescript-eslint/consistent-type-imports
          } as import('fork-ts-checker-webpack-plugin/lib/plugin-options').ForkTsCheckerWebpackPluginOptions
        )
      ] :
      [],
    config.indexHtml && !config.library && !(config.reactNative && !config.reactNativeWeb) ?
      [
        new HtmlWebpackPlugin({
          alwaysWriteToDisk: true,
          baseHref: baseHrefHtml ?? '/',
          filename: path.join(
            '..',
            'index.html'
          ),
          inject: 'body',
          minify: production ?
            {
              collapseWhitespace: false,
              includeAutoGeneratedTags: true,
              keepClosingSlash: true,
              minifyCSS: true,
              minifyJS: true,
              minifyURLs: true,
              removeComments: false,
              removeEmptyAttributes: true,
              removeRedundantAttributes: true,
              removeStyleLinkTypeAttributes: true,
              useShortDoctype: true
            } :
            {},
          scriptLoading: 'defer',
          template: config.indexHtml,
          title: project.title,
          version: project.version
        })
      ] :
      [],
    // eslint-disable-next-line @stylistic/no-extra-parens
    config.library || (config.reactNative && !config.reactNativeWeb) ?
      [] :
      [
        new PreloadWebpackPlugin(),
        new HtmlWebpackHarddiskPlugin()
      ],
    production && !config.library && !(config.reactNative && !config.reactNativeWeb) ?
      [
        new SubresourceIntegrityPlugin({
          enabled: production
        }),
        new WebpackFavicons({
          appDescription: project.description,
          appName: project.title,
          background: '#fff',
          developerName: project.author?.name,
          developerURL: project.author?.url,
          display: 'standalone',
          icons: {
            android: true,
            appleIcon: true,
            appleStartup: true,
            coast: true,
            favicons: true,
            firefox: true,
            windows: true,
            yandex: true
          },
          manifestRelativePaths: true,
          orientation: 'portrait',
          path: path.join(
            'favicons',
            '/'
          ),
          src: config.favicon,
          start_url: baseHrefHtml ?? '/',
          theme_color: '#fff',
          version: project.version
        }),
        new HtmlWebpackLinkTypePlugin()
      ] :
      [],
    config.compression && production && !config.library ?
      [
        new CompressionPlugin({
          algorithm: 'gzip',
          compressionOptions: {
            level: 9
          },
          filename: '[path][name][ext].gz[query]',
          minRatio: 1,
          threshold: 0
        })
        // Wait until LTS
        /*
        new CompressionPlugin({
          algorithm: 'brotliCompress',
          cache: path.join(config.cache, 'compression', 'br'),
          filename: '[path].br[query]',
          minRatio: 1,
          threshold: 0
        })
        */
      ] :
      [],
    config.webpackWorkbox && production && !config.library && !(config.reactNative && !config.reactNativeWeb) ?
      [
        new GenerateSW({
          cacheId: config.shortName,
          clientsClaim: false,
          directoryIndex: 'index.html',
          exclude: [

            /runtime((\.\w+)+)?.js($|\?\w+$)/u
          ],
          // importsDirectory: 'workbox',
          // importWorkboxFrom: 'local',
          include: [
            /\.cs{2}($|\?\w+$)/u,
            /\.html($|\?\w+$)/u,
            /\.js($|\?\w+$)/u
          ],
          modifyURLPrefix: {
            bundle: path.join('.')
          },
          navigateFallback: `${baseHrefHtml ?? '/'}index.html`,
          navigateFallbackDenylist: ([] as RegExp[]).concat(
            [
              /\/bundle\//u,
              /^\/_/u,
              // eslint-disable-next-line regexp/no-super-linear-backtracking
              /\/[^/]+\\.[^/]+$/u
            ]
          ),
          // precacheManifestFilename: 'workbox.manifest.js?_=[manifestHash]',
          runtimeCaching: [
            {
              handler: 'CacheFirst',
              urlPattern: /bundle/u
            }
          ],
          // Must be root
          // https://developers.google.com/web/ilt/pwa/introduction-to-service-worker#registration_and_scope
          swDest: path.join(
            '..',
            'workbox.js'
          )

        })
      ] :
      []
  ),
  profile: true,
  resolve: {
    alias: {
      ...config.react && config.reactNative && config.reactNativeWeb && config.webpackReactNative ?
        {
          'react-native$': 'react-native-web',
          'react-native-vector-icons': 'react-native-vector-icons/dist'
        } :
        {}
    },
    extensions: [
      ...config.reactNative && config.reactNativeWeb && config.webpackReactNative ?
        [
          '.web.js',
          '.web.mjs',
          '.web.jsx',
          '.web.ts',
          '.web.tsx'
        ] :
        [],
      '.js',
      '.mjs',
      '.jsx',
      '.ts',
      '.tsx'
    ],
    modules: [
      path.join('node_modules'),
      path.join(process.cwd())
    ]
  },
  resolveLoader: {
    alias: {
      ...config.react && config.reactNative && config.reactNativeWeb && config.webpackReactNative ?
        {
          'react-native$': 'react-native-web',
          'react-native-vector-icons': 'react-native-vector-icons/dist'
        } :
        {}
    },
    modules: [
      path.join('node_modules'),
      path.join(process.cwd())
    ]
  },
  stats: 'minimal',
  target: config.library ?
    [
      'node'
    ] :
    [
      'web',
      'es5'
    ]
};

module.exports = configuration;
export default configuration;
