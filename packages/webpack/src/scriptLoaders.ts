/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import threadLoader from './threadLoader';
// eslint-disable-next-line import/extensions
import type configuration from './webpack.config';
import {
  config,
  production

} from '@digest/scripts';
import path from 'node:path';

type Loader = typeof configuration.loader;

const {
  cache
} = config;

const scriptLoaders = (
  options: {
    typescript?: boolean;
  } = {}
): Loader[] => {
  return ([] as Loader[]).concat(
    config.parallel ?
      [
        threadLoader
      ] :
      [],
    config.babel &&
      config.webpackBabel ?
      [
        {
          loader: 'babel-loader',
          options: {
            cacheDirectory: cache ?
              path.join(
                cache,
                'babel-loader'
              ) :
              false,
            configFile: config.babel
          }
        }
      ] :
      [],
    options.typescript ?
      ([] as Loader[]).concat(
        !production &&
          config.angular &&
          config.webpackAngular ?
          [
            {
              loader: 'angular2-template-loader'
            },
            {
              loader: '@angularclass/hmr-loader'
            }
          ] :
          []
      ) :
      [],
    [
      {
        loader: 'source-map-loader'
      }
    ]
  );
};

export default scriptLoaders;
