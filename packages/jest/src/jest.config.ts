/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  config
} from '@digest/scripts';
import path from 'node:path';

const babel = config.babel && config.jestBabel;
const cache = config.cache;
const emotion = config.emotion && config.jestEmotion;
const graphql = config.graphql && config.jestGraphQL;
const junit = config.jestJUnit;
const react = config.react && config.jestReact;
const reactNative = react && config.reactNative && config.babelReactNative;
const reactNativeWeb = reactNative && config.reactNativeWeb;
const reports = config.reports;
const typescript = config.typescript && config.jestTypeScript;

const babelPrepPath = babel ?
  // eslint-disable-next-line unicorn/prefer-module,n/no-extraneous-require
  require.resolve('@digest/jest-babel/dist/babelPrep') :
  undefined;

// eslint-disable-next-line unicorn/prefer-module
module.exports = {
  cacheDirectory: cache ?
    path.join(
      cache,
      'jest'
    ) :
    false,
  collectCoverage: Boolean(reports),
  collectCoverageFrom: [
    'src/**/*.js',
    'demo/**/*.js',
    'packages/**/*.js',
    '!**/__e2e__/**/*',
    '!**/__tests__/**/*'
  ].concat(
    react ?
      [
        'src/**/*.jsx',
        'demo/**/*.jsx',
        'packages/**/*.jsx'
      ] :
      [],
    typescript ?
      [
        'src/**/*.ts',
        'demo/**/*.ts',
        'packages/**/*.ts',
        '!src/**/*.d.ts',
        '!demo/**/*.d.ts',
        '!packages/**/*.d.ts'
      ] :
      [],
    react && typescript ?
      [
        'src/**/*.tsx',
        'demo/**/*.tsx',
        'packages/**/*.tsx'
      ] :
      []
  ),
  coverageDirectory: reports ?
    path.join(
      reports,
      'coverage'
    ) :
    undefined,
  coverageProvider: 'v8',
  moduleDirectories: [
    'node_modules',
    '<rootDir>',
    process.cwd()
  ],
  moduleFileExtensions: [
    'json',
    'js',
    'jsx',
    'ts',
    'tsx'
  ],
  moduleNameMapper: {
    '\\.(css|scss|sass|less)$': 'identity-obj-proxy',
    ...reactNativeWeb ?
      {
        '^react-native$': 'react-native-web'
      } :
      {}
  },
  reporters: ([
    'default'
  ] as Array<[string, Record<string, unknown>?] | string>).concat(
    reports && junit ?
      [
        [
          'jest-junit',
          {
            ancestorSeparator: ' › ',
            classNameTemplate: '{classname}',
            outputDirectory: reports,
            outputName: 'junit.xml',
            suiteName: 'jest tests',
            titleTemplate: '{title}',
            usePathForSuiteName: 'true'
          }
        ]
      ] :
      []
  ),
  rootDir: process.cwd(),
  setupFiles: (
    [
      // eslint-disable-next-line unicorn/prefer-module
      require.resolve(path.join(
        '@digest',
        'jest',
        'share',
        'setup.js'
      ))
    ] as string[]
  ).concat(
    config.jestSetup ?
      [
        config.jestSetup
      ] :
      []
  ),
  setupFilesAfterEnv: (
    [] as string[]
  ).concat(
    react ?
      [
        // eslint-disable-next-line unicorn/prefer-module
        require.resolve(path.join(
          '@digest',
          'jest-react',
          'share',
          'env.setup.js'
        ))
      ] :
      []
  ),
  snapshotSerializers: (
    [] as string[]
  ).concat(
    emotion ?
      [
        // eslint-disable-next-line unicorn/prefer-module
        require.resolve(path.join(
          '@emotion',
          'jest',
          'serializer'
        ))
      ] :
      []
  ),
  testEnvironment: 'node',
  testPathIgnorePatterns: [
    '//node_modules//',
    '//lib//',
    '//public//',
    '//static//',
    '//bin//',
    '.d.ts$'
  ],
  // eslint-disable-next-line no-useless-escape
  testRegex: '(__tests__[\\\/\\\\].*|\\.(test|spec))\\.(j|t)sx?$',
  transform: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)(\\?v=[0-9]\\.[0-9]\\.[0-9])?$': 'jest-transform-stub',
    ...babel ?
      {
        '^.+\\.js$': [
          'babel-jest',
          {
            configFile: babelPrepPath
          }
        ]
      } :
      {},
    ...babel && react ?
      {
        '^.+\\.jsx$': [
          'babel-jest',
          {
            configFile: babelPrepPath
          }
        ]
      } :
      {},
    ...babel && typescript ?
      {
        '^.+\\.ts$': [
          'babel-jest',
          {
            configFile: babelPrepPath
          }
        ]
      } :
      {},
    ...babel && react && typescript ?
      {
        '^.+\\.tsx$': [
          'babel-jest',
          {
            configFile: babelPrepPath
          }
        ]
      } :
      {},
    ...babel && graphql ?
      {
        '^.+\\.(gql|graphql)$': 'jest-transform-graphql'
      } :
      {}

  }
};
