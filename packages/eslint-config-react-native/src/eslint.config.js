/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prefer-module,
  canonical/filename-match-exported
*/

const {
  fixupPluginRules
} = require('@eslint/compat');
const reactNativePlugin = require('@react-native-community/eslint-plugin');
// eslint-disable-next-line import/no-extraneous-dependencies,n/no-extraneous-require
const importPlugin = require('eslint-plugin-import');
const eslintReactNativePlugin = require('eslint-plugin-react-native');

const reactConfigs = [
  importPlugin.flatConfigs['react-native'],
  {
    plugins: {
      '@react-native-community': reactNativePlugin,
      'react-native': fixupPluginRules(eslintReactNativePlugin)
    },
    rules: {
      ...eslintReactNativePlugin.configs.all.rules,
      'react-native/no-inline-styles': 'off',
      'react-native/no-raw-text': 'off'
    },
  }
];

module.exports = reactConfigs;
