/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prefer-module
*/

const eslintBabelParser = require('@babel/eslint-parser');
// const eslintBabelPlugin = require('@babel/eslint-plugin');
const {
  config
  // eslint-disable-next-line import/no-extraneous-dependencies,n/no-extraneous-require
} = require('@digest/scripts');

const babel = config.babel && config.eslintBabel;

const babelConfigs = [
  {
    files: [
      '**/*.{js,jsx,mjs,cjs}'
    ],
    languageOptions: {
      parser: eslintBabelParser,
      parserOptions: {
        allowImportExportEverywhere: Boolean(babel),
        babelOptions: {
          configFile: config.babel
        },
        createDefaultProgram: true,
        ecmaVersion: 10,
        requireConfigFile: Boolean(babel),
        sourceType: 'module',
        warnOnUnsupportedTypeScriptVersion: false

        // below significantly slows down linter
        /* 'project': typescript ||
          require.resolve('@digest/typescript/src/tsconfig.json'),
        'tsconfigRootDir': '../../../../'*/
      }
    },
    // rules: eslintBabelPlugin.rulesConfig
  }
];

module.exports = babelConfigs;
