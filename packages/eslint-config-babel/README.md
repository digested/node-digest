<div align="center">
  <img alt="Digest Crane" src="https://gitlab.com/digested/node-digest/raw/master/packages/webpack/share/favicon.png" />
</div><br>

<div align="center">
  <h1>@digest/eslint-config-babel</h1>
</div>

<div align="center">
  <a href="https://commitizen.github.io/cz-cli/"><img alt="Commitizen Friendly" src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000" /></a>
  <a href="https://github.com/semantic-release/semantic-release"><img alt="Semantic Release" src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000" /></a>
</div>

<div align="center">
  <a href="https://gitlab.com/digested/node-digest/-/commits/master" target="_blank">
    <img alt="Status" aria-hidden="" src="https://gitlab.com/digested/node-digest/badges/master/pipeline.svg">
  </a>
  <a href="https://digested.gitlab.io/node-digest/coverage">
    <img alt="Coverage Report" src="https://gitlab.com/digested/node-digest/badges/master/coverage.svg" />
  </a>
  <a href="https://www.npmjs.org/package/@digest/webpack">
    <img alt="NPM Version" src="http://img.shields.io/npm/v/&#64;digest/eslint-config-babel.svg?maxAge=86400" />
  </a>
  <a href="https://www.gnu.org/licenses/lgpl-3.0.en.html">
    <img alt="License LGPL 3.0 or later" src="https://img.shields.io/npm/l/&#64;digest/eslint-config-babel.svg?maxAge=2592000" />
  </a>
  <a href="https://github.com/gajus/eslint-config-canonical">
    <img alt="Code Style Canonical" src="https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000" />
  </a>
</div>
<br />
<br />

> A node digest project package

See the [project page](https://gitlab.com/digested/node-digest/) for documentation or the
[issues](https://gitlab.com/digested/node-digest/-/issues) associated with this package.


