/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

import production from '../production';
import path from 'node:path';

const settings = {
  angular: {
    defaults: true,
    env: 'DIGEST_ANGULAR',
    packageName: '@digest/angular',
    resolve: false,
    type: 'true'
  },
  appHost: {
    defaults: '127.0.0.1',
    env: 'APP_HOST',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  appPort: {
    defaults: production ?
      8_080 :
      8_090,
    env: 'APP_PORT',
    packageName: false,
    resolve: false,
    type: 'number'
  },
  babel: {
    defaults: '@digest/babel/dist/babel.config.js',
    env: 'DIGEST_BABEL',
    packageName: '@digest/babel',
    resolve: true,
    type: 'both'
  },
  babelAngular: {
    defaults: true,
    env: 'DIGEST_BABEL_ANGULAR',
    packageName: '@digest/babel-angular',
    resolve: false,
    type: 'true'
  },
  babelEmotion: {
    defaults: true,
    env: 'DIGEST_BABEL_EMOTION',
    packageName: '@digest/babel-emotion',
    resolve: false,
    type: 'true'
  },
  babelFlow: {
    defaults: true,
    env: 'DIGEST_BABEL_FLOW',
    packageName: '@digest/babel-flow',
    resolve: false,
    type: 'true'
  },
  babelGraphQL: {
    defaults: true,
    env: 'DIGEST_BABEL_GRAPHQL',
    packageName: '@digest/babel-graphql',
    resolve: false,
    type: 'true'
  },
  babelPolyfills: {
    defaults: true,
    env: 'DIGEST_BABEL_POLYFILLS',
    packageName: '@digest/babel-polyfills',
    resolve: true,
    type: 'true'
  },
  babelPreset: {
    defaults: true,
    env: 'DIGEST_BABEL_PRESET',
    packageName: false,
    resolve: false,
    type: 'true'
  },
  babelReact: {
    defaults: true,
    env: 'DIGEST_BABEL_REACT',
    packageName: '@digest/babel-react',
    resolve: false,
    type: 'true'
  },
  babelReactNative: {
    defaults: true,
    env: 'DIGEST_BABEL_REACT_NATIVE',
    packageName: '@digest/babel-react-native',
    resolve: false,
    type: 'true'
  },
  babelTypeScript: {
    defaults: true,
    env: 'DIGEST_BABEL_TYPESCRIPT',
    packageName: '@digest/babel-typescript',
    resolve: false,
    type: 'true'
  },
  baseHref: {
    defaults: '/',
    env: 'DIGEST_BASE_HREF',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  baseHrefHtml: {
    defaults: false,
    env: 'DIGEST_BASE_HREF_HTML',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  cache: {
    defaults: path.join(
      'node_modules',
      '.cache'
    ),
    env: 'DIGEST_CACHE',
    ignore: true,
    packageName: false,
    resolve: false,
    type: 'both'
  },
  compression: {
    defaults: true,
    env: 'DIGEST_COMPRESSION',
    packageName: false,
    resolve: false,
    type: 'true'
  },
  cssModules: {
    defaults: false,
    env: 'DIGEST_CSS_MODULES',
    packageName: false,
    resolve: false,
    type: 'false'
  },
  digestName: {
    defaults: 'digest',
    env: 'DIGEST_NAME',
    packageName: false,
    resolve: true,
    type: 'string'
  },
  e2ePath: {
    defaults: '__e2e__',
    env: 'DIGEST_E2E',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  emotion: {
    defaults: true,
    env: 'DIGEST_EMOTION',
    packageName: '@digest/emotion',
    resolve: false,
    type: 'true'
  },
  env: {
    defaults: true,
    env: 'DIGEST_ENV',
    packageName: false,
    resolve: false,
    type: 'obj'
  },
  eslint: {
    defaults: '@digest/eslint-config/src/.eslintrc.js',
    env: 'DIGEST_ESLINT_ANGULAR',
    ignore: true,
    packageName: '@digest/eslint-config',
    resolve: true,
    type: 'both'
  },
  eslintAngular: {
    defaults: true,
    env: 'DIGEST_ESLINT_ANGULAR',
    packageName: '@digest/eslint-config-angular',
    resolve: false,
    type: 'true'
  },
  eslintBabel: {
    defaults: true,
    env: 'DIGEST_ESLINT_BABEL',
    packageName: '@digest/eslint-config-babel',
    resolve: false,
    type: 'true'
  },
  eslintEmotion: {
    defaults: true,
    env: 'DIGEST_ESLINT_EMOTION',
    packageName: '@digest/eslint-config-emotion',
    resolve: false,
    type: 'true'
  },
  eslintFlow: {
    defaults: true,
    env: 'DIGEST_ESLINT_FLOW',
    packageName: '@digest/eslint-config-flow',
    resolve: false,
    type: 'true'
  },
  eslintGraphQL: {
    defaults: true,
    env: 'DIGEST_ESLINT_GRAPHQL',
    packageName: '@digest/eslint-config-graphql',
    resolve: false,
    type: 'true'
  },
  eslintJest: {
    defaults: true,
    env: 'DIGEST_ESLINT_JEST',
    packageName: '@digest/eslint-config-jest',
    resolve: false,
    type: 'true'
  },
  eslintReact: {
    defaults: true,
    env: 'DIGEST_ESLINT_REACT',
    packageName: '@digest/eslint-config-react',
    resolve: false,
    type: 'true'
  },
  eslintReactNative: {
    defaults: true,
    env: 'DIGEST_ESLINT_REACT_NATIVE',
    packageName: '@digest/eslint-config-react-native',
    resolve: false,
    type: 'true'
  },
  //  eslintReactNative: {defaults: true, resolve: false, packageName: '@digest/eslint-config-react-native', type: 'true', env: 'DIGEST_ESLINT_REACT_NATIVE'},
  eslintTypeScript: {
    defaults: true,
    env: 'DIGEST_ESLINT_TYPESCRIPT',
    packageName: '@digest/eslint-config-typescript',
    resolve: false,
    type: 'true'
  },
  favicon: {
    defaults: '@digest/webpack/share/favicon.png',
    env: 'DIGEST_FAVICON',
    ignore: true,
    packageName: false,
    resolve: true,
    type: 'both'
  },
  flow: {
    defaults: true,
    env: 'DIGEST_FLOW',
    ignore: true,
    packageName: '@digest/flow',
    resolve: false,
    type: 'both'
  },
  graphql: {
    defaults: true,
    env: 'DIGEST_GRAPHQL',
    ignore: true,
    packageName: '@digest/graphql',
    resolve: false,
    type: 'both'
  },
  graphqlSchema: {
    defaults: 'schema.graphql',
    env: 'DIGEST_GRAPHQL_SCHEMA',
    packageName: false,
    resolve: true,
    type: 'string'
  },
  hashLimit: {
    defaults: 6,
    env: 'DIGEST_HASH_LIMIT',
    packageName: false,
    resolve: false,
    type: 'number'
  },
  indexHtml: {
    defaults: '@digest/webpack/share/index.htm',
    env: 'DIGEST_INDEX_HTML',
    packageName: false,
    resolve: true,
    type: 'string'
  },
  jest: {
    defaults: true,
    env: 'DIGEST_JEST',
    packageName: '@digest/jest',
    resolve: false,
    type: 'true'
  },
  jestBabel: {
    defaults: true,
    env: 'DIGEST_JEST_BABEL',
    packageName: '@digest/jest-babel',
    resolve: false,
    type: 'true'
  },
  jestEmotion: {
    defaults: true,
    env: 'DIGEST_JEST_EMOTION',
    packageName: '@digest/jest-emotion',
    resolve: false,
    type: 'true'
  },
  jestGraphQL: {
    defaults: true,
    env: 'DIGEST_JEST_GRAPHQL',
    packageName: '@digest/jest-graphql',
    resolve: false,
    type: 'true'
  },
  jestJUnit: {
    defaults: true,
    env: 'DIGEST_JEST_JUNIT',
    packageName: '@digest/jest-junit',
    resolve: false,
    type: 'true'
  },
  jestReact: {
    defaults: true,
    env: 'DIGEST_JEST_REACT',
    packageName: '@digest/jest-react',
    resolve: false,
    type: 'true'
  },
  jestSetup: {
    defaults: '@digest/jest/dist/jest.setup.js',
    env: 'DIGEST_JEST_SETUP',
    packageName: '@digest/jest-setup',
    resolve: false,
    type: 'string'
  },
  jestTypeScript: {
    defaults: true,
    env: 'DIGEST_JEST_TYPESCRIPT',
    packageName: '@digest/jest-typescript',
    resolve: false,
    type: 'true'
  },
  library: {
    defaults: false,
    env: 'DIGEST_LIBRARY',
    packageName: false,
    resolve: false,
    type: 'false'
  },
  minimize: {
    defaults: true,
    env: 'DIGEST_MINIMIZE',
    packageName: false,
    resolve: false,
    type: 'true'
  },
  parallel: {
    defaults: true,
    env: 'DIGEST_PARALLEL',
    packageName: false,
    resolve: false,
    type: 'true'
  },
  polyfills: {
    defaults: 'polyfills.js',
    env: 'DIGEST_POLYFILLS',
    ignore: true,
    packageName: false,
    resolve: true,
    type: 'both'
  },
  postcss: {
    defaults: '@digest/postcss/src/postcss.config.js',
    env: 'DIGEST_POSTCSS',
    ignore: true,
    packageName: '@digest/postcss',
    resolve: true,
    type: 'both'
  },
  postcssWhitelist: {
    defaults: false,
    env: 'DIGEST_POSTCSS_WHITELIST',
    packageName: false,
    resolve: true,
    type: 'string'
  },
  pwa: {
    defaults: true,
    env: 'DIGEST_PWA',
    packageName: false,
    resolve: false,
    type: 'bool'
  },
  react: {
    defaults: true,
    env: 'DIGEST_REACT',
    packageName: '@digest/react',
    resolve: false,
    type: 'true'
  },
  reactNative: {
    defaults: true,
    env: 'DIGEST_REACT_NATIVE',
    packageName: '@digest/react-native',
    resolve: false,
    type: 'true'
  },
  reactNativeWeb: {
    defaults: false,
    env: 'DIGEST_REACT_NATIVE_WEB',
    packageName: false,
    resolve: false,
    type: 'false'
  },
  reports: {
    defaults: 'reports',
    env: 'DIGEST_REPORTS',
    ignore: true,
    packageName: false,
    resolve: false,
    type: 'both'
  },
  // https://chromedriver.storage.googleapis.com/index.html
  seleniumChromeDriver: {
    defaults: '111.0.5563.64',
    env: 'SELENIUM_CHROME_DRIVER',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  // https://github.com/SeleniumHQ/selenium/releases/
  seleniumDriver: {
    defaults: '4.8.0',
    env: 'SELENIUM_DRIVER',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  // https://github.com/mozilla/geckodriver/releases
  seleniumGeckoDriver: {
    defaults: '0.33.0',
    env: 'SELENIUM_GECKO_DRIVER',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  seleniumHeadless: {
    defaults: false,
    env: 'SELENIUM_HEADLESS',
    packageName: false,
    resolve: false,
    type: 'bool'
  },
  seleniumHost: {
    defaults: '127.0.0.1',
    env: 'SELENIUM_HOST',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  seleniumPort: {
    defaults: 4_444,
    env: 'SELENIUM_PORT',
    packageName: false,
    resolve: false,
    type: 'number'
  },
  seleniumStart: {
    defaults: '',
    env: 'SELENIUM_START',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  shortName: {
    defaults: 'digest',
    env: 'DIGEST_SHORT_NAME',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  sourceMaps: {
    defaults: true,
    env: 'DIGEST_SOURCE_MAPS',
    packageName: false,
    resolve: false,
    type: 'true'
  },
  sourcePath: {
    defaults: 'src',
    env: 'DIGEST_SOURCE_PATH',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  staticPath: {
    defaults: 'public',
    env: 'DIGEST_STATIC_PATH',
    packageName: false,
    resolve: false,
    type: 'string'
  },
  stylelint: {
    defaults: '@digest/stylelint-config/src/.stylelintrc.json',
    env: 'DIGEST_STYLELINT',
    packageName: '@digest/stylelint-config',
    resolve: true,
    type: 'both'
  },
  stylelintIgnore: {
    defaults: '@digest/stylelint-config/src/.stylelintignore',
    env: 'DIGEST_STYLELINT_IGNORE',
    packageName: false,
    resolve: true,
    type: 'string'
  },
  typescript: {
    defaults: 'tsconfig.json',
    env: 'DIGEST_TYPESCRIPT',
    packageName: '@digest/typescript',
    resolve: true,
    type: 'true'
  },
  webpack: {
    defaults: 'webpack.config.js',
    env: 'DIGEST_WEBPACK',
    packageName: '@digest/typescript',
    resolve: true,
    type: 'string'
  },
  webpackAngular: {
    defaults: true,
    env: 'DIGEST_WEBPACK_ANGULAR',
    packageName: '@digest/webpack-angular',
    resolve: false,
    type: 'true'
  },
  webpackBabel: {
    defaults: true,
    env: 'DIGEST_WEBPACK_BABEL',
    packageName: '@digest/webpack-babel',
    resolve: false,
    type: 'true'
  },
  webpackDashboard: {
    defaults: true,
    env: 'DIGEST_WEBPACK_DASHBOARD',
    packageName: '@digest/webpack-dashboard',
    resolve: false,
    type: 'true'
  },
  webpackESLint: {
    defaults: true,
    env: 'DIGEST_WEBPACK_ESLINT',
    packageName: '@digest/webpack-eslint',
    resolve: false,
    type: 'true'
  },
  webpackFlow: {
    defaults: true,
    env: 'DIGEST_WEBPACK_FLOW',
    packageName: '@digest/webpack-flow',
    resolve: false,
    type: 'true'
  },
  webpackGraphQL: {
    defaults: true,
    env: 'DIGEST_WEBPACK_GRAPHQL',
    packageName: '@digest/webpack-graphql',
    resolve: false,
    type: 'true'
  },
  webpackPostCSS: {
    defaults: true,
    env: 'DIGEST_WEBPACK_POSTCSS',
    packageName: '@digest/webpack-postcss',
    resolve: false,
    type: 'true'
  },
  webpackReact: {
    defaults: true,
    env: 'DIGEST_WEBPACK_REACT',
    packageName: '@digest/webpack-react',
    resolve: false,
    type: 'true'
  },
  webpackReactNative: {
    defaults: true,
    env: 'DIGEST_WEBPACK_REACT_NATIVE',
    packageName: '@digest/webpack-react-native',
    resolve: false,
    type: 'true'
  },
  webpackStyleLint: {
    defaults: true,
    env: 'DIGEST_WEBPACK_STYLELINT',
    packageName: '@digest/webpack-stylelint',
    resolve: false,
    type: 'true'
  },
  webpackTypeScript: {
    defaults: true,
    env: 'DIGEST_WEBPACK_TYPESCRIPT',
    packageName: '@digest/webpack-typescript',
    resolve: false,
    type: 'true'
  },
  webpackWorkbox: {
    defaults: true,
    env: 'DIGEST_WEBPACK_WORKBOX',
    packageName: '@digest/webpack-workbox',
    resolve: false,
    type: 'true'
  }
} as {
  [key: string]: {
    defaults: boolean | number | string;
    env: string;
    ignore?: boolean;
    packageName: false | string;
    resolve: boolean;
    type: 'bool' | 'both' | 'false' | 'number' | 'obj' | 'string' | 'true';
  };
};

const keys = Object.keys(settings) as Array<keyof completeDigest>;

type completeDigest = {
  angular: boolean;
  appHost: string;
  appPort: number;
  babel: false | string;
  babelAngular: boolean;
  babelEmotion: boolean;
  babelFlow: boolean;
  babelGraphQL: boolean;
  babelPolyfills: string;
  babelPreset: boolean;
  babelReact: boolean;
  babelReactNative: boolean;
  babelTypeScript: boolean;
  baseHref: string;
  baseHrefHtml: false | string;
  cache: false | string;
  compression: boolean;
  context: string;
  cssModules: boolean;
  digestName: string;
  e2ePath: string;
  emotion: boolean;
  env: { [key: string]: number | string };
  eslint: false | string;
  eslintAngular: boolean;
  eslintBabel: boolean;
  eslintEmotion: boolean;
  eslintFlow: boolean;
  eslintGraphQL: boolean;
  eslintJest: boolean;
  eslintReact: boolean;
  eslintReactNative: boolean;
  eslintTypeScript: boolean;
  favicon: false | string;
  flow: false | string;
  graphql: boolean;
  graphqlSchema: string;
  hashLimit: number;
  indexHtml: false | string;
  jest: boolean;
  jestBabel: boolean;
  jestEmotion: boolean;
  jestGraphQL: boolean;
  jestJUnit: boolean;
  jestReact: boolean;
  jestSetup: string;
  jestTypeScript: boolean;
  library: boolean;
  minimize: boolean;
  parallel: boolean;
  polyfills: false | string;
  postcss: string;
  postcssWhitelist: string;
  pwa: boolean;
  react: boolean;
  reactNative: boolean;
  reactNativeWeb: boolean;
  reports: false | string;
  seleniumChromeDriver: string;
  seleniumDriver: string;
  seleniumGeckoDriver: string;
  seleniumHeadless: boolean;
  seleniumHost: string;
  seleniumPort: number;
  seleniumStart: boolean;
  shortName: string;
  sourceMaps: boolean;
  sourcePath: string;
  staticPath: string;
  stylelint: false | string;
  stylelintIgnore: string;
  typescript: string;
  webpack: string;
  webpackAngular: boolean;
  webpackBabel: boolean;
  webpackDashboard: boolean;
  webpackESLint: boolean;
  webpackFlow: boolean;
  webpackGraphQL: boolean;
  webpackPostCSS: boolean;
  webpackReact: boolean;
  webpackReactNative: boolean;
  webpackStyleLint: boolean;
  webpackTypeScript: boolean;
  webpackWorkbox: boolean;
};

type digest = Omit<Partial<completeDigest>, 'context'> & {
  angular?: false;
  babel?: false | string;
  babelAngular?: false;
  babelEmotion?: false;
  babelFlow?: false;
  babelGraphQL?: false;
  babelPolyfills?: false;
  babelPreset?: false;
  babelReact?: false;
  babelReactNative?: false;
  babelTypeScript?: false;
  cache?: false | string;
  compression?: false;
  cssModules?: true;
  eslint?: false | string;
  eslintAngular?: false;
  eslintBabel?: false;
  eslintEmotion?: false;
  eslintFlow?: false;
  eslintGraphQL?: false;
  eslintJest?: false;
  eslintReact?: false;
  eslintReactNative?: false;
  eslintTypeScript?: false;
  favicon?: false | string;
  flow?: false | string;
  graphql?: false;
  jest?: false;
  jestBabel?: false;
  jestEmotion?: false;
  jestGraphQL?: false;
  jestJUnit?: false;
  jestReact?: false;
  jestTypeScript?: false;
  library?: true;
  minimize?: false;
  parallel?: false;
  polyfills?: false | string;
  postcss?: false | string;
  pwa?: true;
  react?: false;
  reactNative?: false;
  reactNativeWeb?: true;
  reports?: false | string;
  sourceMaps?: false;
  stylelint?: false | string;
  typescript?: false | string;
  webpackAngular?: false;
  webpackBabel?: false;
  webpackDashboard?: false;
  webpackESLint?: false;
  webpackFlow?: false;
  webpackGraphQL?: false;
  webpackPostCSS?: false;
  webpackReact?: false;
  webpackReactNative?: false;
  webpackStyleLint?: false;
  webpackTypeScript?: false;
  webpackWorkbox?: false;
};

export default settings;

export {
  keys
};

export type {
  completeDigest,
  digest
};
