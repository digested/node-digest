/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
const getBool = (
  environment: boolean | string | undefined,
  ignore?: boolean
) => {
  // eslint-disable-next-line lodash/prefer-lodash-typecheck,@stylistic/no-extra-parens
  if (environment === 'true' || (typeof environment === 'boolean' && environment === true)) {
    if (ignore === undefined) {
      return true;
    } else if (ignore) {
      return undefined;
    } else {
      return true;
    }
  // eslint-disable-next-line lodash/prefer-lodash-typecheck,@stylistic/no-extra-parens
  } else if (environment === 'false' || (typeof environment === 'boolean')) {
    if (ignore === undefined) {
      return false;
    } else if (ignore) {
      return false;
    } else {
      return undefined;
    }
  } else if (environment === '') {
    return undefined;
  } else {
    return environment;
  }
};

export default getBool;
