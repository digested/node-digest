/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prevent-abbreviations,
  @typescript-eslint/no-require-imports
*/
import project from '../package';
import production from '../production';
import consume from './consume';
import resolvePath from './resolvePath';
import settings, {
  type completeDigest,
  type digest,
  keys
} from './settings';
import findUp from 'find-up';
import path from 'node:path';

const cwd = process.cwd();

const {
  DIGEST_NAME = 'digest.config.json'
  // eslint-disable-next-line n/no-process-env
} = process.env;

const digestPath = findUp.sync(DIGEST_NAME);
const digestConfig: digest = digestPath ?
  // eslint-disable-next-line unicorn/prefer-module
  require(digestPath) :
  {};

// eslint-disable-next-line unicorn/no-array-reduce
const config = keys.reduce<Partial<completeDigest>>(
  (
    prev,
    key
  ) => {
    const {
      defaults,
      env: envKey,
      packageName,
      resolve,
      type
    } = settings[key];

    /**
     * Step 1: Resolve user given setting
     */
    // Get user environment settings
    // eslint-disable-next-line n/no-process-env
    const envSetting = process.env[envKey];

    const transformedEnvSetting = consume(
      type,
      envSetting
    );

    const configSetting = digestConfig[key as keyof digest];

    const transformedConfigSetting = consume(
      type,
      configSetting
    );

    // environment settings are priority over configuration settings
    const userSetting = transformedEnvSetting ?? transformedConfigSetting ?? defaults;

    /**
     * Step 2: If setting relies on a package, check if installed
     */
    // Set setting to false if package is not enabled
    const verifiedSetting = userSetting &&
      packageName ?
      // eslint-disable-next-line @stylistic/no-extra-parens
      (resolvePath(packageName) ?
        userSetting :
        false) :
      userSetting;

    /**
     * Step 3: If setting requires resolving the file path; attempt
     */
    const resolveSetting = verifiedSetting &&
      resolve ?
      resolvePath(
        verifiedSetting,
        true
      ) :
      verifiedSetting;

    /**
     * Step 4: Special cases
     */
    // eslint-disable-next-line @typescript-eslint/switch-exhaustiveness-check
    switch (key) {
      case 'cache': {
        prev[key] = resolveSetting ?
          path.join(
            cwd,
            resolveSetting
          ) :
          resolveSetting;

        break;
      }

      case 'reports': {
        prev[key] = resolveSetting === 'reports' ?
          path.join(
            cwd,
            'reports'
          ) :
          resolveSetting;

        break;
      }

      case 'shortName': {
        prev[key] = transformedEnvSetting ??
          transformedConfigSetting ??
          project?.name?.replace(
            '@',
            ''
          )?.replace(
            '/',
            '-'
          ) ?? 'nd';

        break;
      }

      default: {
        prev[key] = resolveSetting as never;
      }
    }

    return prev;
  },
  {
    digestName: DIGEST_NAME
  }
) as completeDigest;

/**
 * Step 5: Special cases dependant on other settings values
 */
config.cache = config.cache ?
  path.join(
    config.cache,
    config.library ?
      'library' :
      // eslint-disable-next-line @stylistic/no-extra-parens
      (
        production ?
          'production' :
          'development'
      )
  ) :
  config.cache;

config.staticPath = config.staticPath === 'public' ?
  path.join(
    cwd,
    config.library ?
      'lib' :
      'public'
  ) :
  config.staticPath;

config.context = path.join(
  process.cwd(),
  config.sourcePath as string
);

export default config;
