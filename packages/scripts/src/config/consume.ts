/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import getBool from './getBool';

/**
 * Depending on user setting and type transform accordingly
 */
const consume = (
  type: 'bool' | 'both' | 'false' | 'number' | 'obj' | 'string' | 'true',
  setting: unknown
) => {
  switch (type) {
    case 'bool': {
      return getBool(setting as boolean);
    }

    case 'both': {
      return getBool(
        setting as boolean,
        true
      );
    }

    case 'false': {
      return getBool(
        setting as boolean,
        false
      );
    }

    case 'number': {
      const number = Number(setting);

      return Number.isNaN(number) ?
        undefined :
        number;
    }

    case 'obj': {
      if (
        setting &&
        Object.prototype.toString.call(setting) === '[object String]'
      ) {
        // eslint-disable-next-line unicorn/prefer-string-replace-all
        return JSON.parse((setting as string).replace(
          /""(.*)""/gu,
          '"\\"$1\\""'
        ));
      } else {
        return setting || {};
      }
    }

    case 'true': {
      return getBool(
        setting as boolean,
        true
      );
    }

    case 'string':
    default: {
      return setting;
    }
  }
};

export default consume;
