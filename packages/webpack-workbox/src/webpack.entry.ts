/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-regex
*/
const baseHref = document.querySelector('base') === null ?
  '/' :
// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  document.querySelector('base')!.href || '/';

if ('serviceWorker' in navigator) {
  window.addEventListener(
    'load',
    (): void => {
      void navigator.serviceWorker.register(`${baseHref}workbox.js`);

      /*
      .then(function (registration) {
        console.info('SW registered: ', registration); // eslint-disable-line no-console
      }).catch(function (error) {
        console.error('SW registration failed: ', error); // eslint-disable-line no-console
      });/**/
    }
  );
}

export {};
