/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prefer-module
*/
const {
  config
} = require('@digest/scripts');
const {
  getDefaultConfig
} = require('metro-config');
const exclusionList = require('metro-config/src/defaults/exclusionList');
const {
  getMetroAndroidAssetsResolutionFix,
  getMetroTools
} = require('react-native-monorepo-tools');

const monorepoMetroTools = getMetroTools();

const androidAssetsResolutionFix = getMetroAndroidAssetsResolutionFix();

module.exports = (
  async () => {
    const {
      resolver: {
        sourceExts
      }
    } = await getDefaultConfig();

    return {
      resolver: {
        blacklistRE: exclusionList([
          /.*web.[jt]sx?/u
        ]),
        // Ensure we resolve nohoist libraries from this directory.
        blockList: exclusionList(monorepoMetroTools.blockList),
        extraNodeModules: monorepoMetroTools.extraNodeModules,
        sourceExts: [
          ...sourceExts,
          'css'
        ]
      },
      server: {
        // ...and to the server middleware.
        enhanceMiddleware: (middleware) => {
          return androidAssetsResolutionFix.applyMiddleware(middleware);
        }
      },
      transformer: {
        babelTransformerPath: config.postcss ?
          require.resolve('./postcss-transformer.js') :
          require.resolve('react-native-css-transformer'),
        getTransformOptions: async () => {
          return {
            transform: {
              experimentalImportSupport: false,
              inlineRequires: false
            }
          };
        },
        // Apply the Android assets resolution fix to the public path...
        publicPath: androidAssetsResolutionFix.publicPath
      },
      // Add additional Yarn workspace package roots to the module map.
      // This allows importing importing from all the project's packages.
      watchFolders: monorepoMetroTools.watchFolders
    };
  }
)();
