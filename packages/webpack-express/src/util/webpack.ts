/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  config as digestConfig
} from '@digest/scripts';
// eslint-disable-next-line import/no-extraneous-dependencies
import webpackSetup, {
  type Configuration
// eslint-disable-next-line n/no-extraneous-import
} from 'webpack';

const webpack = (
  dashboard = false
) => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
  const config = require(digestConfig.webpack) as Configuration;

  if (
    dashboard
  ) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module,n/no-extraneous-require, import/no-extraneous-dependencies
    const DashboardPlugin = require('webpack-dashboard/plugin');

    config.plugins?.concat(
      new DashboardPlugin()
    );
  }

  const compiler = webpackSetup(config);

  return compiler;
};

export default webpack;
