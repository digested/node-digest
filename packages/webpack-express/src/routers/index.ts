/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prevent-abbreviations
*/
import webpack from '../util/webpack';
import dev from './dev';
import hot from './hot';
import history from '@digest/express/dist/routers/history';
import statics from '@digest/express/dist/routers/statics';
import {
  config,
  production
} from '@digest/scripts';
// eslint-disable-next-line import/no-extraneous-dependencies
import {
  Router as expressRouter,
  type Router
  // eslint-disable-next-line n/no-extraneous-import
} from 'express';

const {
  baseHref
} = config;

const staticPath = config.staticPath;

const routers = expressRouter({
  caseSensitive: true,
  strict: true
}) as Router;

const historyRouter = history(
  baseHref
) as Router;

const staticsRouter = statics(
  baseHref,
  staticPath,
  production
) as Router;

routers.use(
  baseHref,
  historyRouter,
  staticsRouter
);

if (!production) {
  const compiler = webpack();

  const devRouter = dev(
    compiler,
    baseHref
  );

  const hotRouter = hot(
    compiler,
    baseHref
  );

  /**
   * on Express, only path that works for non-root
   * webpack-dev-middleware and webpack-hot-middleware :-/
   */
  const routersBase = `${baseHref}*`;

  routers.get(
    routersBase,
    devRouter,
    hotRouter
  );
}

export default routers;
