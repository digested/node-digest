/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prevent-abbreviations
*/
// eslint-disable-next-line import/no-extraneous-dependencies
import {
  Router as expressRouter,
  type Router
  // eslint-disable-next-line n/no-extraneous-import
} from 'express';
import path from 'node:path';
import {
  type Compiler
  // eslint-disable-next-line n/no-extraneous-import
} from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';

const dev = (
  compiler: Compiler,
  baseHref: string,
  publicPath = 'bundle'
): Router => {
  const router = expressRouter({
    caseSensitive: true,
    strict: true
  });

  const devMiddleware = webpackDevMiddleware(
    compiler,
    {
      publicPath: path.join(
        baseHref,
        publicPath
      ),
      stats: {
        colors: true
      }
    }
  );

  const devBase = '/*';

  router.get(
    devBase as string,
    devMiddleware
  );

  router.head(
    devBase,
    devMiddleware
  );

  router.options(
    devBase,
    devMiddleware
  );

  return router;
};

export default dev;
