/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
// eslint-disable-next-line import/no-extraneous-dependencies
import {
  Router as expressRouter,
  type Router
  // eslint-disable-next-line n/no-extraneous-import
} from 'express';
import path from 'node:path';
import {
  type Compiler
  // eslint-disable-next-line n/no-extraneous-import
} from 'webpack';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import webpackHotMiddleware from 'webpack-hot-middleware';

const hot = (
  compiler: Compiler,
  baseHref: string,
  hotPath = 'hot'
): Router => {
  const router = expressRouter({
    caseSensitive: true,
    strict: true
  });

  const hotMiddleware = webpackHotMiddleware(
    compiler,
    {
      path: `${path.join(
        baseHref
      )}${hotPath}`
    }
  );

  const hotBase = '/*';

  router.get(
    hotBase,
    hotMiddleware
  );

  router.head(
    hotBase,
    hotMiddleware
  );

  router.options(
    hotBase,
    hotMiddleware
  );

  return router;
};

export default hot;
