/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import routers from './routers';
import server from '@digest/express/dist/server';
import {
  production
} from '@digest/scripts';
// eslint-disable-next-line import/no-extraneous-dependencies,n/no-extraneous-import
import slashes from 'connect-slashes';
// eslint-disable-next-line import/no-extraneous-dependencies
import express, {
  type Express,
  type RequestHandler
  // eslint-disable-next-line n/no-extraneous-import
} from 'express';
// eslint-disable-next-line import/no-extraneous-dependencies
import helmet, {
  contentSecurityPolicy
  // eslint-disable-next-line n/no-extraneous-import
} from 'helmet';

const app = express() as Express;

app.use(
  helmet(),
  routers,
  slashes(false) as RequestHandler
);

if (!production) {
  app.use(contentSecurityPolicy({
    directives: {
      defaultSrc: [
        '\'self\'',
        '\'unsafe-inline\''
      ],
      upgradeInsecureRequests: null
    }
  }));
}

server(
  app
);

export default app;
