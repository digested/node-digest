/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-exported
*/
import {
  type ConfigAPI,
  type PluginItem,
  type TransformOptions
} from '@babel/core';
import {
  config,
  production
} from '@digest/scripts';

const angular = config.angular && config.babelAngular;
const emotion = config.emotion && config.babelEmotion;
const flow = config.flow && config.babelFlow;
const graphql = config.graphql && config.babelGraphQL;
const react = config.react && config.babelReact;
const reactNative = react && config.reactNative && config.babelReactNative;
const reactNativeWeb = reactNative && config.reactNativeWeb;
const typescript = config.typescript && config.babelTypeScript;

const babelConfig = function (
  api?: ConfigAPI
): TransformOptions {
  if (api) {
    api.cache.forever();
  }

  const plugins: PluginItem[] = (
    [
      // Stage 0
      [
        '@babel/plugin-proposal-function-bind'
      ],

      // Stage 1
      [
        '@babel/plugin-proposal-export-default-from'
      ],
      [
        '@babel/plugin-transform-logical-assignment-operators'
      ],
      [
        '@babel/plugin-transform-optional-chaining',
        {
          loose: false
        }
      ],
      [
        '@babel/plugin-proposal-pipeline-operator',
        {
          proposal: 'minimal'
        }
      ],
      [
        '@babel/plugin-transform-nullish-coalescing-operator',
        {
          loose: false
        }
      ],
      [
        '@babel/plugin-proposal-do-expressions'
      ],

      // Stage 2
      [
        '@babel/plugin-proposal-function-sent'
      ],
      [
        '@babel/plugin-transform-export-namespace-from'
      ],
      [
        '@babel/plugin-transform-numeric-separator'
      ],
      [
        '@babel/plugin-proposal-throw-expressions'
      ],

      // Stage 3
      [
        '@babel/plugin-syntax-import-meta'
      ]
    ] as PluginItem[]
  ).concat(
    typescript ?
      [
        [
          '@babel/plugin-transform-typescript'
        ]
      ] :
      [],
    [
      [
        '@babel/plugin-transform-class-properties',
        {
          loose: false
        }
      ],
      [
        '@babel/plugin-proposal-decorators',
        {
          legacy: true
        }
      ]
    ],
    config.webpackBabel ?
      [
        [
          '@babel/plugin-syntax-dynamic-import'
        ]
      ] :
      [],
    production ?
      [
        [
          'babel-plugin-transform-node-env-inline'
        ]
      ] :
      [],
    emotion ?
      [
        [
          '@emotion'
        ]
      ] :
      [],
    react ?
      [
        [
          '@babel/plugin-transform-react-display-name'
        ]
      ] :
      [],
    reactNativeWeb ?
      [
        [
          'module-resolver',
          {
            alias: {
              '^react-native$': 'react-native-web'
            }
          }
        ],
        [
          'react-native-web'
        ]
      ] :
      [],
    angular ?
      [] :
      [],
    [
      [
        '@babel/plugin-transform-destructuring'
      ],
      [
        '@babel/plugin-transform-runtime',
        {
          helpers: false,
          regenerator: true
        }
      ],
      [
        '@babel/plugin-transform-regenerator',
        {
          async: false
        }
      ],
      [
        '@babel/plugin-transform-classes'
      ]
    ],
    config.cssModules &&
      react ?
      [
        [
          '@dr.pogodin/babel-plugin-react-css-modules',
          {
            context: config.context,
            generateScopedName: config.library ?
              `${config.shortName}-[local]` :
              `[local]-[hash:${config.hashLimit}]`
          }
        ]
      ] :
      [],
    production &&
      react &&
      !config.library ?
      [
        // Breaks react-hot-loader, only use in production and NOT for libraries
        [
          '@babel/plugin-transform-react-inline-elements'
        ]
      ] :
      [],
    production &&
      react ?
      [
        [
          'babel-plugin-react-remove-properties',
          {
            properties: [
              'data-test',
              'data-test-id',
              'data-testid'
            ]
          }
        ],
        [
          'babel-plugin-transform-react-remove-prop-types',
          {
            removeImport: true
          }
        ],
        [
          '@babel/plugin-transform-react-constant-elements',
          {
            allowMutablePropsOnTags: [
              'FormattedMessage'
            ]
          }
        ]
      ] :
      [],
    !production &&
      react &&
      config.webpackBabel ?
      [
        // eslint-disable-next-line n/no-extraneous-require,unicorn/prefer-module
        require.resolve('react-refresh/babel')
      ] :
      [],
    production &&
      graphql ?
      [
        [
          'babel-plugin-graphql-tag'
        ]
      ] :
      []
  );

  const presets = ([] as PluginItem[]).concat(
    config.babelPreset ?
      [
        [
          '@babel/preset-env',
          {
            corejs: 3,
            debug: false,
            modules: 'auto',
            shippedProposals: true,
            useBuiltIns: 'usage'
          }
        ]
      ] :
      [],
    react ?
      [
        [
          '@babel/preset-react',
          {
            development: !production,
            runtime: 'automatic',
            useBuiltIns: false
          }
        ]
      ] :
      [],
    typescript ?
      [
        [
          '@babel/preset-typescript',
          {}
        ]
      ] :
      [],
    flow ?
      [
        [
          '@babel/preset-flow',
          {}
        ]
      ] :
      []
  );

  return {
    plugins,
    presets
  };
};

// module.exports must be used to play nice with some
// other babel config consumers (eg. Jest)

// eslint-disable-next-line unicorn/prefer-module
module.exports = babelConfig;
