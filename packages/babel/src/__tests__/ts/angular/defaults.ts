/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import scripts from '../../scripts.json';
import {
  type PluginTarget,
  type TransformOptions
} from '@babel/core';
import {
  config
} from '@digest/scripts';
import {
  beforeAll,
  expect,
  jest
} from '@jest/globals';

const PLUGINS = [
  ...scripts.ts.angular.plugins
];

let babelConfig: TransformOptions;
let plugins: PluginTarget[];

describe(
  'Webpack settings for Angular',
  (): void => {
    beforeAll(
      (): void => {
        jest.resetModules();
        // eslint-disable-next-line n/no-process-env
        process.env = {
          DIGEST_ANGULAR: 'true'
        };

        if (config.babel) {
          // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
          babelConfig = require(config.babel)();
        }

        if (babelConfig.plugins) {
          plugins = babelConfig.plugins.map(
            (plugin): PluginTarget => {
              if (Object.prototype.toString.call(plugin) === '[object String]') {
                return plugin;
              }

              if (Array.isArray(plugin)) {
                return plugin[0];
              }

              return '';
            }
          );
        }
      }
    );

    it(
      'plugins',
      (): void => {
        expect(plugins).toStrictEqual(expect.arrayContaining(PLUGINS));
      }
    );
  }
);
