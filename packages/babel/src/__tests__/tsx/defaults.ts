/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import scripts from '../scripts.json';
import {
  type PluginTarget,
  type TransformOptions
} from '@babel/core';
import {
  config
} from '@digest/scripts';
import {
  beforeAll,
  expect,
  jest
} from '@jest/globals';

const PRESETS = [
  ...scripts.jsx.presets
];

let babelConfig: TransformOptions;
let presets: PluginTarget[];

describe(
  'Webpack settings for React and Typescript',
  (): void => {
    beforeAll(
      (): void => {
        jest.resetModules();
        // eslint-disable-next-line n/no-process-env
        process.env = {
          DIGEST_ANGULAR: 'false'
        };

        if (config.babel) {
          // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
          babelConfig = require(config.babel)();
        }

        if (babelConfig.presets) {
          presets = babelConfig.presets.map(
            (preset): PluginTarget => {
              return Array.isArray(preset) ?
                preset[0] :
                preset;
            }
          );
        }
      }
    );

    it(
      'presets',
      (): void => {
        expect(presets).toStrictEqual(expect.arrayContaining(PRESETS));
      }
    );
  }
);
