/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  type PluginTarget,
  type TransformOptions
} from '@babel/core';
import {
  beforeAll,
  expect,
  jest
} from '@jest/globals';

const DIGEST_SHORT_NAME = 'test-[local]';

let babelConfig: TransformOptions;
let cssModulesPlugin: string;

describe(
  'Webpack cssModules settings for library on',
  (): void => {
    beforeAll(
      (): void => {
        // eslint-disable-next-line n/no-process-env
        process.env = {
          DIGEST_CSS_MODULES: 'true',
          DIGEST_LIBRARY: 'true',
          DIGEST_SHORT_NAME: 'test'
        };
        // eslint-disable-next-line n/no-process-env
        process.env.DIGEST_SHORT_NAME = 'test';
        jest.resetModules();

        const {
          config
          // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
        } = require('@digest/scripts');

        if (config.babel) {
          // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
          babelConfig = require(config.babel)();
        }

        if (babelConfig.plugins) {
          cssModulesPlugin = (
            babelConfig.plugins.find(
              (plugin): boolean => {
                return Array.isArray(plugin) && plugin[0] === '@dr.pogodin/babel-plugin-react-css-modules';
              }
            ) as [PluginTarget, { generateScopedName: string }]
          )[1].generateScopedName;
        }
      }
    );

    it(
      'custom short name',
      (): void => {
        expect(cssModulesPlugin).toStrictEqual(DIGEST_SHORT_NAME);
      }
    );
  }
);
