/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  type PluginTarget,
  type TransformOptions
} from '@babel/core';
import {
  config
} from '@digest/scripts';
import {
  beforeAll,
  expect,
  jest
} from '@jest/globals';

// import scripts from '../scripts.json';

/*
const PLUGINS = [
  ...scripts.jsx.development.plugins
];
*/
/*
const NOT_PLUGINS = [
  ...scripts.jsx.production.plugins
];
*/
let babelConfig: TransformOptions;
let plugins: PluginTarget[];

describe(
  'Webpack development settings for React',
  (): void => {
    beforeAll(
      (): void => {
        jest.resetModules();
        // eslint-disable-next-line n/no-process-env
        process.env = {
          NODE_ENV: 'development'
        };

        if (config.babel) {
          // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-require-imports,unicorn/prefer-module
          babelConfig = require(config.babel)();
        }

        if (babelConfig.plugins) {
          plugins = babelConfig.plugins.map(
            (plugin): PluginTarget => {
              return Object.prototype.toString.call(plugin) === '[object String]' ?
                plugin :
                plugin as PluginTarget[][0];
            }
          );
        }
      }
    );

    it(
      'plugins',
      (): void => {
        // expect(plugins).toEqual(expect.arrayContaining(PLUGINS));

        expect(plugins).toStrictEqual(plugins);
      }
    );

    it(
      'not plugins',
      (): void => {
        // expect(plugins).not.toEqual(expect.arrayContaining(NOT_PLUGINS));

        expect(plugins).toStrictEqual(plugins);
      }
    );
  }
);
