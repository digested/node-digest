/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prefer-module,
  canonical/filename-match-exported,
  no-unused-vars
*/

const compat = require('@eslint/compat');
const canonicalBrowser = require('eslint-config-canonical/configurations/browser');
const canonical = require('eslint-config-canonical/configurations/canonical');
const canonicalJSDoc = require('eslint-config-canonical/configurations/jsdoc');
const canonicalJSON = require('eslint-config-canonical/configurations/json');
const canonicalLodash = require('eslint-config-canonical/configurations/lodash');
const canonicalModule = require('eslint-config-canonical/configurations/module');
const canonicalNode = require('eslint-config-canonical/configurations/node');
const canonicalRegExp = require('eslint-config-canonical/configurations/regexp');
const fs = require('node:fs');
const path = require('node:path');

const gitignorePath = path.resolve(__dirname, '.gitignore');
const gitignoreExists = fs.existsSync(gitignorePath);

const {
  'import/no-cycle': omit1,
  'import/no-deprecated': omit2,
  'import/no-extraneous-dependencies': noExraneousDependencies,
  ...canonicalFixedRules
} = canonical.recommended.rules;

const {
  'n/no-extraneous-import': noExraneousImport,
  ...canonicalNodeFixedRules
} = canonicalNode.recommended.rules;

const {
  'jsonc/sort-keys': sortKeys,
  ...canonicalJSONFixedRules
} = canonicalJSON.recommended.rules;

const generalConfigs = [
  gitignoreExists ?
    [
      compat.includeIgnoreFile(gitignorePath)
    ] :
    [],
  [
    {
      ignores: [
        '**/dist/*',
        '**/node_modules/*',
        '**/public/*'
      ],
    },
    {
      ...canonical.recommended,
      rules: {
        ...canonicalFixedRules,
        '@stylistic/comma-dangle': 'off'
      }
    },
    {
      ...canonical.recommended,
      ignores: [
        '**/__tests__/*'
      ],
      rules: {
        '@stylistic/comma-dangle': 'off',
        'import/no-extraneous-dependencies': noExraneousDependencies
      }
    },
    {
      ...canonicalNode.recommended,
      rules: canonicalNodeFixedRules
    },
    {
      ...canonicalNode.recommended,
      ignores: [
        '**/__tests__/*'
      ],
      rules: {
        'n/no-extraneous-import': noExraneousImport
      }
    },
    canonicalModule.recommended,
    canonicalBrowser.recommended,
    canonicalRegExp.recommended,
    canonicalJSDoc.recommended,
    canonicalLodash.recommended,
    {
      ...canonicalJSON.recommended,
      rules: canonicalJSONFixedRules
    },
    {
      ...canonicalJSON.recommended,
      ignores: [
        '**/package.json'
      ],
      rules: {
        'jsonc/sort-keys': sortKeys
      }
    }
  ]
].flat();

module.exports = generalConfigs;
