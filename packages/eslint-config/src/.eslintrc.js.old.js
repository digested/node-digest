/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  canonical/filename-match-regex,
  unicorn/prefer-module
*/
const {
  config
} = require('@digest/scripts');
const fs = require('node:fs');
const path = require('node:path');

const angular = config.angular && config.eslintAngular;
const babel = config.babel && config.eslintBabel;
const emotion = config.emotion && config.eslintEmotion;
const flow = config.flow && config.eslintFlow;
const graphql = config.graphql && config.eslintGraphQL;
const jest = config.jest && config.eslintJest;
const react = config.react && config.eslintReact;
const reactNative = react && config.reactNative && config.eslintReactNative;
const typescript = config.typescript && config.eslintTypeScript;

let graphqlSchema;

if (graphql && config.graphqlSchema) {
  // eslint-disable-next-line n/no-deprecated-api
  require.extensions['.graphql'] = function (module, filename) {
    module.exports = fs.readFileSync(filename, 'utf8');
  };

  try {
    // eslint-disable-next-line import/no-dynamic-require, n/global-require
    graphqlSchema = require(path.resolve(config.graphqlSchema) ?? '');
  } catch {
    graphqlSchema = undefined;
  }
}

module.exports = {
  env: {
    browser: true,
    commonjs: true,
    jest: Boolean(jest),
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'canonical',
    'canonical/lodash',
    'canonical/browser',
    'canonical/json',
    'canonical/module',
    'canonical/node',
    'canonical/yaml'
  ].concat(
    babel ? [
      'plugin:import/stage-0'
    ] : [],
    jest ? [
      'canonical/jest'
    ] : [],
    react ? [
      'plugin:react/recommended',
      'plugin:react-hooks/recommended',
      'plugin:jsx-a11y/recommended',
      'plugin:import/react',
      'canonical/react',
      'canonical/jsx-a11y',
      'plugin:css-modules/recommended'
    ] : [],
    /*
    graphql ? [
      'canonical/graphql'
    ] : [],/**/
    flow && !typescript ? [
      'plugin:flowtype/recommended',
      'canonical/flowtype'
    ] : [],
    typescript ? [
      'plugin:@typescript-eslint/eslint-recommended',
      'plugin:@typescript-eslint/recommended',
      'plugin:@typescript-eslint/recommended-requiring-type-checking',
      'plugin:import/typescript',
      'plugin:canonical/recommended',
      'canonical/typescript'
    ] : []
  ),
  globals: {
    // angular: Boolean(angular),
    define: true
  },
  ignorePatterns: [
    '.eslintrc.js',
    '*~.js',
    '*~.ts'
  ],
  overrides: [
    {
      files: [
        '*.graphql'
      ],
      parser: '@graphql-eslint/eslint-plugin',
      plugins: [
        '@graphql-eslint'
      ]
    },
    {
      files: [
        '*.json'
      ],
      parser: 'eslint-plugin-json-es'
    }
  ],
  parser: typescript ?
    '@typescript-eslint/parser' :
    // eslint-disable-next-line @stylistic/no-extra-parens
    (babel ?
      '@babel/eslint-parser' :
      undefined),
  parserOptions: {
    allowImportExportEverywhere: Boolean(babel),
    babelOptions: babel ?
      {
        configFile: config.babel
      } :
      undefined,
    createDefaultProgram: true,
    ecmaFeatures: {
      jsx: Boolean(react)
    },
    ecmaVersion: 10,
    project: typescript ?
      config.typescript :
      undefined,
    requireConfigFile: Boolean(babel),
    sourceType: 'module',
    warnOnUnsupportedTypeScriptVersion: false

    // below significantly slows down linter
    /* 'project': typescript ||
      require.resolve('@digest/typescript/src/tsconfig.json'),
    'tsconfigRootDir': '../../../../'*/
  },
  plugins: [
    'import',
    'canonical'
  ].concat(
    babel ? [
      '@babel'
    ] : [],
    emotion ? [
      '@emotion'
    ] : [],
    angular ? [] : [],
    react ? [
      'react',
      'react-hooks',
      'jsx-a11y',
      'css-modules'
    ] : [],
    reactNative ? [
      'react-native',
      '@react-native-community'
    ] : [],
    graphqlSchema ? [
      '@graphql-eslint'
    ] : [],
    flow ? [
      'flowtype'
    ] : [],
    typescript ? [
      '@typescript-eslint'
    ] : []
  ),
  rules: {
    'comma-dangle': [
      'error',
      'never'
    ],
    'function-call-argument-newline': [
      'error',
      'always'
    ],
    'function-paren-newline': [
      'error',
      'multiline-arguments'
    ],
    'import/no-extraneous-dependencies': 'off',
    'import/no-unassigned-import': 'off',
    'import/no-unresolved': 'off',
    // incompatible with @jest-environment
    'jsdoc/check-tag-names': 'off',
    'multiline-ternary': [
      'error',
      'always'
    ],
    'no-tabs': 'off',
    ...typescript ? {
      '@typescript-eslint/comma-dangle': [
        'error',
        'never'
      ],
      '@typescript-eslint/indent': [
        'error',
        'tab'
      ],
      '@typescript-eslint/member-delimiter-style': [
        'error',
        {
          multiline: {
            delimiter: 'semi',
            requireLast: true
          }
        }
      ],
      '@typescript-eslint/restrict-plus-operands': 'off',
      indent: 'off',
      'no-use-before-define': 'off',
      'no-useless-constructor': 'off'
    } : {
      indent: [
        'error',
        'tab'
      ]
    },
    ...react ? {
      'react/forbid-component-props': 'off',
      'react/jsx-fragments': [
        'error',
        'syntax'
      ],
      'react/jsx-indent': [
        'error',
        'tab'
      ],
      'react/jsx-indent-props': [
        'error',
        'tab'
      ],
      'react/jsx-sort-default-props': 'off',
      'react/jsx-wrap-multilines': [
        'error',
        {
          arrow: 'parens-new-line',
          assignment: 'parens-new-line',
          condition: 'parens-new-line',
          declaration: 'parens-new-line',
          logical: 'parens-new-line',
          prop: 'parens-new-line',
          return: 'parens-new-line'
        }
      ],
      'react/no-unknown-property': [
        'error',
        {
          ignore: [
            'styleName'
          ]
        }
      ],
      'react/sort-default-props': 'error'
    } : {},
    ...jest ? {
      'jest/no-test-callback': 'off'
    } : {},
    ...graphqlSchema ? {
      'graphql/template-strings': [
        'error',
        {
          env: 'apollo',
          schemaString: graphqlSchema
        }
      ]
    } : {}
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: true
    },
    'import/ignore': [
      '\\.(scss|less|css)$'
    ],
    'import/parsers': {

      ...typescript ? {
        '@typescript-eslint/parser': [
          '.ts',
          '.tsx'
        ]
      } : {}
    },
    propWrapperFunctions: [
      'forbidExtraProps'
    ],
    react: {
      flowVersion: flow && !typescript ? 'detect' : undefined,
      version: react ? 'detect' : undefined
    }
  }
};
