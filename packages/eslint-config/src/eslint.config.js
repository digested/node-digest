/*
  eslint-disable
  unicorn/prefer-module,
  canonical/filename-match-exported
*/
/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

const generalConfigs = require('./general.eslint.config');
const yamlConfigs = require('./yaml.eslint.config');
const {
  config: {
    eslintBabel,
    eslintGraphQL,
    eslintJest,
    eslintReact,
    eslintReactNative,
    eslintTypeScript
  }
} = require('@digest/scripts');

const getBabelConfig = () => {
  if (eslintBabel) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-babel');
  } else {
    return [];
  }
};

const getReactConfig = () => {
  if (eslintReact) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-react');
  } else {
    return [];
  }
};

const getReactNativeConfig = () => {
  if (eslintReact && eslintReactNative) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-react-native');
  } else {
    return [];
  }
};

const getTypeScriptConfig = () => {
  if (eslintTypeScript) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-typescript');
  } else {
    return [];
  }
};

const getGraphQLConfig = () => {
  if (eslintGraphQL) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-graphql');
  } else {
    return [];
  }
};

const getJestConfig = () => {
  if (eslintJest) {
    // eslint-disable-next-line n/global-require, n/no-extraneous-require, import/no-extraneous-dependencies
    return require('@digest/eslint-config-jest');
  } else {
    return [];
  }
};

const configs = [
  ...generalConfigs,
  ...yamlConfigs,
  ...getReactConfig(),
  ...getReactNativeConfig(),
  ...getBabelConfig(),
  ...getTypeScriptConfig(),
  ...getGraphQLConfig(),
  ...getJestConfig()
];

module.exports = configs;
