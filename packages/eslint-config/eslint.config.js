/*
  eslint-disable
  canonical/filename-match-exported,
  unicorn/prefer-module
*/

const config = require('./src/eslint.config');

module.exports = config;
