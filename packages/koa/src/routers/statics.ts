/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import StaticMiddleware from '../middleware/statics';
import Router from '@koa/router';
import mount from 'koa-mount';
import {
  type Options
} from 'koa-static';

const statics = (
  baseHref: string,
  staticPath: string,
  production = false,
  serveStaticOptions?: Options
) => {
  const router = new Router({
    sensitive: true,
    strict: true
  });

  const staticMiddleware = StaticMiddleware(
    baseHref,
    staticPath,
    production,
    serveStaticOptions
  );

  const staticBase = '(.*)';

  router.get(
    staticBase,
    mount(
      baseHref,
      staticMiddleware
    )
  );

  router.head(
    staticBase,
    mount(
      baseHref,
      staticMiddleware
    )
  );

  router.options(
    staticBase,
    mount(
      baseHref,
      staticMiddleware
    )
  );

  return router;
};

export default statics;
