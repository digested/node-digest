/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  type Options
  // eslint-disable-next-line n/no-extraneous-import
} from 'connect-history-api-fallback';
import {
  type Middleware
} from 'koa';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import koaHistory from 'koa-history-api-fallback';
import path from 'node:path';

const history = (
  baseHref?: string,
  options?: Options
) => {
  const ACCEPT_HEADERS = [
    'text/html',
    'application/xhtml+xml'
  ];

  const defaultIndex = baseHref ?
    path.join(
      baseHref,
      'index.html'
    ) :
    undefined;

  const defaultOptions = {
    htmlAcceptHeaders: ACCEPT_HEADERS,
    index: defaultIndex,
    ...options
  };

  const middleware = koaHistory(defaultOptions);

  const historyMiddleware: Middleware = async (
    context,
    next
  ) => {
    // Do not run history middleware more than once
    if (context.state.history) {
      // eslint-disable-next-line n/callback-return
      await next();
    } else {
      const nextRoute = async () => {
        context.state.history = true;

        await context.router.routes()(
          context,
          next
        );
      };

      await middleware(
        context,
        nextRoute
      );
    }
  };

  return historyMiddleware;
};

export default history;
