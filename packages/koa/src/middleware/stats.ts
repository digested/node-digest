/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import helmet from 'koa-helmet';

const stats = () => {
  const helmetMiddleware = helmet.contentSecurityPolicy({
    directives: {
      'script-src': [
        '\'self\'',
        '\'unsafe-inline\''
      ]
    },
    useDefaults: true
  });

  return helmetMiddleware;
};

export default stats;
