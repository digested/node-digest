/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import koaStatic, {
  type Options
} from 'koa-static';

const MAX_AGE = 31_536_000;

const defaultServeStaticOptions: Options = {
  maxAge: MAX_AGE,
  setHeaders: (
    response,
    filePath
  ) => {
    if (
      filePath.includes('workbox.js')
    ) {
      response.setHeader(
        'Cache-Control',
        'max-age=0, no-cache, no-store, must-revalidate'
      );
      response.setHeader(
        'Pragma',
        'no-cache'
      );
      response.setHeader(
        'Expires',
        '0'
      );
    }
  }
};

const statics = (
  baseHref: string,
  staticPath: string,
  production = false,
  serveStaticOptions?: Options
) => {
  const options: Options = {
    brotli: production,
    defer: true,
    gzip: production,
    ...defaultServeStaticOptions,
    ...serveStaticOptions
  };

  const staticMiddleware = koaStatic(
    staticPath,
    options
  );

  return staticMiddleware;
};

export default statics;
