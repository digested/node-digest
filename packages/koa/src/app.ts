/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import Routers from './routers';
import server from './server';
import {
  production
} from '@digest/scripts';
import Koa from 'koa';
import helmet from 'koa-helmet';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
import slashes from 'koa-remove-trailing-slashes';

const app = new Koa();

const router = Routers();

app.use(
  helmet()
).use(
  router.routes()
).use(
  router.allowedMethods()
).use(
  slashes()
);

if (!production) {
  app.use(
    helmet.contentSecurityPolicy({
      directives: {
        defaultSrc: [
          '\'self\'',
          '\'unsafe-inline\''
        ],
        upgradeInsecureRequests: null
      }
    })
  );
}

server(app);

export default app;
