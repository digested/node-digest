/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  Router as expressRouter,
  type NextFunction,
  type Request,
  type Response,
  type Router
} from 'express';
import expressStaticGzip from 'express-static-gzip';
import expressStatic, {
  type ServeStaticOptions
} from 'serve-static';

const MAX_AGE = 31_536_000;

const defaultServeStaticOptions: ServeStaticOptions = {
  immutable: true,
  index: false,
  lastModified: false,
  maxAge: MAX_AGE,
  redirect: false,
  setHeaders: (
    response,
    filePath
  ) => {
    if (
      filePath.includes('workbox.js')
    ) {
      response.setHeader(
        'Cache-Control',
        'max-age=0, no-cache, no-store, must-revalidate'
      );
      response.setHeader(
        'Pragma',
        'no-cache'
      );
      response.setHeader(
        'Expires',
        '0'
      );
    }
  }
};

// Part of what koa-mount does
const removeBaseHref = (
  middleware: (
    request: Request,
    response: Response,
    next: NextFunction
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ) => any,
  baseHref?: string
) => {
  return (
    request: Request,
    response: Response,
    next: NextFunction
  ) => {
    const fixedRequest = {
      headers: request.headers,
      method: request.method,
      originalUrl: baseHref ?
        request.originalUrl.replace(
          baseHref,
          '/'
        ) :
        request.originalUrl,
      path: baseHref ?
        request.path.replace(
          baseHref,
          '/'
        ) :
        request.path,
      url: baseHref ?
        request.url.replace(
          baseHref,
          '/'
        ) :
        request.url
    };

    return middleware(
      fixedRequest as Request,
      response,
      next
    );
  };
};

const statics = (
  baseHref: string,
  staticPath: string,
  production = false,
  serveStaticOptions?: ServeStaticOptions
): Router => {
  const router = expressRouter({
    caseSensitive: true,
    strict: true
  });

  const options = {
    ...defaultServeStaticOptions,
    ...serveStaticOptions
  };

  const staticBase = '/*';

  const serveStatic = removeBaseHref(
    // expressStaticGzip does not play nice with webpack-dev-middleware
    production ?
      expressStaticGzip(
        staticPath,
        {
          enableBrotli: true,
          index: false,
          serveStatic: options
        }
      ) :
      expressStatic(
        staticPath,
        options
      ),
    baseHref
  );

  router.get(
    staticBase,
    serveStatic
  );

  router.head(
    staticBase,
    serveStatic
  );

  return router;
};

export default statics;
