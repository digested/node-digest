/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import history from './history';
import statics from './statics';
import stats from './stats';
import {
  config,
  production
} from '@digest/scripts';
import {
  Router as expressRouter,
  type Router
} from 'express';

const {
  baseHref
} = config;

const staticPath = config.staticPath;

const routers = expressRouter({
  caseSensitive: true,
  strict: true
}) as Router;

const statsRouter = stats();

const historyRouter = history(
  baseHref
);

const staticsRouter = statics(
  baseHref,
  staticPath,
  production
);

routers.use(
  baseHref,
  statsRouter,
  historyRouter,
  staticsRouter
);

export default routers;
