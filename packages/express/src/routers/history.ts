/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import historyFallback from 'connect-history-api-fallback';
import {
  Router as expressRouter,
  type RequestHandler,
  type Router
} from 'express';
import path from 'node:path';

const ACCEPT_HEADERS = [
  'text/html',
  'application/xhtml+xml'
];

const history = (
  baseHref = '/'
): Router => {
  const router = expressRouter({
    caseSensitive: true,
    strict: true
  });

  const defaultIndex = path.join(
    baseHref,
    'index.html'
  );

  const historyMiddleware = historyFallback({
    htmlAcceptHeaders: ACCEPT_HEADERS,
    index: defaultIndex
  }) as RequestHandler;

  const historyBase = '/*';

  router.get(
    historyBase,
    historyMiddleware
  );

  router.head(
    historyBase,
    historyMiddleware
  );

  router.options(
    historyBase,
    historyMiddleware
  );

  return router;
};

export default history;
