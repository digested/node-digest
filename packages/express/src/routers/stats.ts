/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
import {
  Router as expressRouter,
  type Router
} from 'express';
import {
  contentSecurityPolicy
} from 'helmet';

const stats = (): Router => {
  const router = expressRouter({
    caseSensitive: true,
    strict: true
  });

  const helmetMiddleware = contentSecurityPolicy({
    directives: {
      'script-src': [
        '\'self\'',
        '\'unsafe-inline\''
      ]
    },
    useDefaults: true
  });

  const statsPath = '/stats/*';

  router.get(
    statsPath,
    helmetMiddleware
  );

  router.head(
    statsPath,
    helmetMiddleware
  );

  router.options(
    statsPath,
    helmetMiddleware
  );

  return router;
};

export default stats;
