/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/
/*
  eslint-disable
  unicorn/prefer-module
*/
const config = require('@digest/scripts');
const postcssEach = require('postcss-each');
const postcssImport = require('postcss-import');
const postcssMixins = require('postcss-mixins');
const postcssNormalize = require('postcss-normalize');
const postcssPresetEnv = require('postcss-preset-env');
const postcssReporter = require('postcss-reporter');

let stylelint;

if (config.stylelintConfig) {
  // eslint-disable-next-line n/no-extraneous-require, n/global-require, import/no-extraneous-dependencies
  stylelint = require('stylelint');
}

module.exports = ({
  // env,
  // file,
  options
}) => {
  return {
    plugins: [].concat(
      config.stylelintConfig ?
        [
          stylelint({
            configFile: config.stylelintConfig,
            emitErrors: true,
            ignorePath: config.stylelintIgnore,
            ...options.lint
          })
        ] :
        [],
      [
        postcssNormalize({
          browsers: [
            'last 2 versions',
            'safari 7',
            'ie 9'
          ]
        }),
        postcssPresetEnv({
          stage: 0,
          ...options.env
        }),
        postcssImport(options.imp),
        postcssMixins(options.mixins),
        postcssEach(options.each),
        postcssReporter({
          clearReportedMessages: true,
          throwError: true,
          ...options.reporter
        })
      ]
    )
  };
};
