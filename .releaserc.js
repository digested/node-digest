module.exports = {
	branches: [
		'+([0-9])?(.{+([0-9]),x}).x',
		{
			name: 'master',
			channel: undefined
		},
		{
			name: 'next',
			prerelease: true,
			channel: 'next'
		},
		{
			name: 'beta',
			prerelease: true,
			channel: 'beta'
		}
	],
	ci: true,
	plugins: [
		"@semantic-release/commit-analyzer",
		[
			'semantic-release-lerna',
			{
				'generateNotes': false,
				'npmPublish': true,
				'rootVersion': false,
				'latch': 'prerelease'
			}
		],
		[
      '@semantic-release/git',
      {
        'assets': [
          'lerna.json',
					'package.json',
          'demo/*/package.json',
          'packages/*/package.json'
        ]
      }
    ]
	]
}
