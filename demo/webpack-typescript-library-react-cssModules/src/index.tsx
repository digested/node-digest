/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

/*
  eslint-disable
  canonical/filename-match-exported
*/

// eslint-disable-next-line import/no-unassigned-import
import './vendor';
import ClassName from './ClassName';
import StyleName from './StyleName';
import React, {
  Component,
  type ReactElement
} from 'react';

// eslint-disable-next-line react/prefer-stateless-function
export default class App extends Component<Record<string, unknown>, Record<string, unknown>> {
  public render (): ReactElement {
    return (
      <div>
        <ClassName title='className' />
        <StyleName title='styleName' />
      </div>
    );
  }
}
