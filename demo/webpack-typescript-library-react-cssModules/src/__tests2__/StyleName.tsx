/**
 * @jest-environment jsdom
 */

/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

/*
  eslint-disable
  import/no-extraneous-dependencies,
  n/no-extraneous-import
*/

import StyleName from '../StyleName';
import {
  expect
} from '@jest/globals';
import {
  render
} from '@testing-library/react';
import React from 'react';

describe(
  'App',
  () => {
    it(
      'StyleName',
      (): void => {
        const component = render(
          <StyleName title='StyleName Test' />
        );
        const app = component.asFragment();
        expect(app).toMatchSnapshot();
      }
    );
  }
);
