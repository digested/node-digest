/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

declare module '*.css';
declare module '*.scss';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
declare let System: {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  import: any;
};

declare module '*.json' {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const value: any;
  // eslint-disable-next-line @typescript-eslint/prefer-ts-expect-error
  // @ts-ignore
  export default value;
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
declare let BUILD: {
  DATE: Date;
};
