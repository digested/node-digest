import React, {
  type PropsWithChildren
} from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    marginTop: 32,
    paddingLeft: 24,
    paddingRight: 24
  },
  // eslint-disable-next-line react-native/no-color-literals
  description: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    marginTop: 8
  },
  // eslint-disable-next-line react-native/no-color-literals
  title: {
    color: 'white',
    fontSize: 24,
    fontWeight: '600'
  }
});

const Section: React.FC<
  PropsWithChildren<{
    readonly title: string;
  }>
> = ({
  children,
  title
}) => {
  return (
    <View
      style={styles.container}
    >
      <Text
        style={styles.title}
      >
        {title}
      </Text>
      <Text
        style={
          styles.description
        }
      >
        {children}
      </Text>
    </View>
  );
};

export default Section;
