import React, {
  lazy,
  StrictMode,
  Suspense
} from 'react';
import {
  Text
} from 'react-native';

// eslint-disable-next-line n/no-process-env
if (process.env.NODE_ENV === 'development') {
  /*
  // eslint-disable-next-line promise/prefer-await-to-then
  import('react-dom').then(
    (ReactDOM): void => {
      // @ts-ignore
      return import('react-axe').then( // eslint-disable-line promise/prefer-await-to-then
        // eslint-disable-next-line no-undef,@typescript-eslint/no-explicit-any
        (axe: any): void => {
          return axe(React, ReactDOM, 5000, {
            rules: [{
              enabled: false,
              id: 'region'
            }]
          });
        }
      ).catch((): void => {});
    }
  ).catch((): void => {});

  /*
  // eslint-disable-next-line no-undef,promise/prefer-await-to-then,@typescript-eslint/no-explicit-any
  import('@welldone-software/why-did-you-render').then((whyDidYouRender: any): void => {
    return whyDidYouRender(
      React,
      {
        collapseGroups: true,
        groupByComponent: true,
        include: [/^Dynamic$/]
      }
    );
  }).catch((): void => {});

  /**/
}

const Lazy = lazy(
  // eslint-disable-next-line @typescript-eslint/consistent-type-imports, @typescript-eslint/promise-function-async
  (): Promise<typeof import('./App')> => {
    return import(

      /* webpackChunkName: "Providers" */
      /* webpackPrefetch: true */
      './App'
    );
  }
);

const App = () => {
  return (
    <StrictMode>
      <Suspense
        fallback={(
          <Text>
            Loading...
          </Text>
        )}
      >
        <Lazy />
      </Suspense>
    </StrictMode>
  );
};

export default App;
