/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 */

import Section from './Section';
import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View
} from 'react-native';

const styles = StyleSheet.create({
  background: {
    flexGrow: 1
  },
  // eslint-disable-next-line react-native/no-color-literals
  background2: {
    backgroundColor: 'red'
  },
  highlight: {
    fontWeight: '700'
  }
});

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  return (
    <SafeAreaView
      style={styles.background}
    >
      <StatusBar
        barStyle={isDarkMode ?
          'light-content' :
          'dark-content'}
      />
      <ScrollView
        contentInsetAdjustmentBehavior='automatic'
        style={styles.background}
      >
        <View
          style={styles.background2}
        >
          <Section
            title='Step One'
          >
            <Text>
              Edit <Text
                style={styles.highlight}
              >
                App.tsx
              </Text> to change this
              screen and then come back to see your edits.
            </Text>
          </Section>
          <Section
            title='Learn More'
          >
            <Text>
              Read the docs to discover what to do next:
            </Text>
          </Section>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default App;
