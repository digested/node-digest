/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

import ClassName from './ClassName';
import StyleName from './StyleName';
import React, {
  Component,
  type ReactElement
} from 'react';

// eslint-disable-next-line react/prefer-stateless-function
class App extends Component<Record<string, unknown>, Record<string, unknown>> {
  public render (): ReactElement {
    void import(

      /* webpackChunkName: "Chunk" */
      /* webpackPrefetch: true */
      './Chunk'
    // eslint-disable-next-line promise/prefer-await-to-then
    ).then(
      (): void => {
      // console.log(chunk.default); // eslint-disable-line no-console
      }
    );

    return (
      <div>
        <ClassName title='className' />
        <StyleName title='styleName' />
      </div>
    );
  }
}

export default App;
