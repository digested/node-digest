/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

import * as styles from './index.css';
import React, {
  type ReactElement
} from 'react';

const ClassName = ({
  title = 'className default'
}: NestedProps): ReactElement => {
  return (
    <div>
      <div className={styles['app-hyphen']}>
        {title}
      </div>
    </div>
  );
};

export default ClassName;

export type NestedProps = {
  readonly title: string;
};
