/*

  This file is part of the Node Digest.

  Node Digest is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your option)
  any later version.

  Node Digest is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
  for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with Node Digest . If not, see <https://www.gnu.org/licenses/>.

*/

module.exports = {
  'React smoke test': async function (browser) {
    return await browser
      .resizeWindow(1024, 768)
      .url('http://localhost:8080/node-digest/builds/webpack-react/index.html')
      .waitForElementVisible('body', 1000)
      .assert.screenshotIdenticalToBaseline('body')
      .end();
  }
};
