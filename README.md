<div align="center">
	<img alt="Digest Crane" src="https://gitlab.com/digested/node-digest/raw/master/packages/webpack/share/favicon.png" />
</div>
<br>

<div align="center">
	<h1>@digest</h1>
</div>

<div align="center">
	<a href="https://commitizen.github.io/cz-cli/"><img alt="Commitizen Friendly" src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?maxAge=2592000" /></a>
	<a href="https://github.com/semantic-release/semantic-release"><img alt="Semantic Release" src="https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?maxAge=2592000" /></a>
</div>

<div align="center">
	<a href="https://gitlab.com/digested/node-digest/commits/master"><img alt="Build Status" src="https://gitlab.com/digested/node-digest/badges/master/pipeline.svg" /></a>
	<a href="https://digested.gitlab.io/node-digest/coverage"><img alt="Coverage Report" src="https://gitlab.com/digested/node-digest/badges/master/coverage.svg" /></a>
	<a href="https://www.npmjs.org/package/@digest/scripts"><img alt="NPM Version" src="http://img.shields.io/npm/v/@digest/scripts.svg?maxAge=86400" /></a>
	<a href="https://www.gnu.org/licenses/lgpl-3.0.en.html"><img alt="License" src="https://img.shields.io/npm/l/@digest/scripts.svg?maxAge=2592000" /></a>
	<a href="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fnode-digest.svg?type=shield"><img alt="FOSSA Status" src="https://app.fossa.io/api/projects/git%2Bhttps%3A%2F%2Fgitlab.com%2Fdigested%2Fnode-digest.svg?type=shield&maxAge=3600" /></a>
	<a href="https://github.com/gajus/canonical"><img alt="Canonical Code Style" src="https://img.shields.io/badge/code%20style-canonical-blue.svg?maxAge=2592000" /></a>
</div>

<h2> </h2>

The **`@digest`** project maintains collections of reusable and
*extendable* configurations. Many of these configurations are *package-aware* and adjust their configuration based on neighbouring `@digest` dependencies.

> "Covention over configuration"

## Contents

###### [<mark>Home</mark>](https://gitlab.com/digested/node-digest/-/wikis/Home)
###### [<mark>Settings</mark>](https://gitlab.com/digested/node-digest/-/wikis/Settings)
###### *<mark>Packages</mark>*

- <mark>@digest/angular</mark>
- [<mark>@digest/babel</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8babel)
- <mark>@digest/babel‑angular</mark>
- <mark>@digest/babel‑emotion</mark>
- <mark>@digest/babel‑flow</mark>
- <mark>@digest/babel‑graphql</mark>
- <mark>@digest/babel‑polyfills</mark>
- <mark>@digest/babel‑react</mark>
- <mark>@digest/babel‑react-native</mark>
- <mark>@digest/babel‑typescript</mark>
- [<mark>@digest/eslint‑config</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8eslint‑config)
- <mark>@digest/emotion</mark>
- <mark>@digest/eslint‑config‑angular</mark>
- <mark>@digest/eslint‑config‑babel</mark>
- <mark>@digest/eslint‑config‑emotion</mark>
- <mark>@digest/eslint‑config‑flow</mark>
- <mark>@digest/eslint‑config‑graphql</mark>
- <mark>@digest/eslint‑config‑jest</mark>
- <mark>@digest/eslint‑config‑react</mark>
- <mark>@digest/eslint‑config‑typescript</mark>
- [<mark>@digest/express</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8express)
- <mark>@digest/flow</mark>
- <mark>@digest/graphql</mark>
- [<mark>@digest/jest</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8jest)
- <mark>@digest/jest‑babel</mark>
- <mark>@digest/jest‑emotion</mark>
- <mark>@digest/jest‑graphql</mark>
- <mark>@digest/jest‑junit</mark>
- <mark>@digest/jest‑react</mark>
- <mark>@digest/jest‑typescript</mark>
- [<mark>@digest/koa</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8koa)
- [<mark>@digest/nightwatch</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8nightwatch)
- <mark>@digest/postcss</mark>
- <mark>@digest/react</mark>
- <mark>@digest/react-native</mark>
- [<mark>@digest/scripts</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8scripts)
- [<mark>@digest/stylelint‑config</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8stylelint‑config)
- [<mark>@digest/typescript</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8typescript)
- [<mark>@digest/webpack</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8webpack)
- <mark>@digest/webpack‑angular</mark>
- <mark>@digest/webpack‑babel</mark>
- <mark>@digest/webpack‑dashboard</mark>
- <mark>@digest/webpack‑eslint</mark>
- [<mark>@digest/webpack‑express</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8webpack‑express)
- <mark>@digest/webpack‑flow</mark>
- <mark>@digest/webpack‑graphql</mark>
- [<mark>@digest/webpack‑koa</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8webpack‑koa)
- <mark>@digest/webpack‑postcss</mark>
- <mark>@digest/webpack‑react</mark>
- <mark>@digest/webpack‑stylelint</mark>
- [<mark>@digest/webpack‑typescript</mark>](https://gitlab.com/digested/node-digest/-/wikis/@digest/@digest%E2%A7%B8webpack‑typescript)
- <mark>@digest/webpack‑workbox</mark>


## License

[`LGPLv3`](https://www.gnu.org/licenses/lgpl-3.0.en.html)
