var path = require('path');
var rimraf = require('rimraf');

var types = path.join(
	require.resolve('html-webpack-harddisk-plugin'),
	'..',
	'index.d.ts'
);

console.log('Removed: ' + types)

rimraf.sync(types);
